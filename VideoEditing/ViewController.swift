//
//  ViewController.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 17.09.2021.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    var videoURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func gallery(_ sender: UIButton) {
        let picker = UIImagePickerController()
        picker.delegate = self
//        picker.sourceType = .photoLibrary
        picker.sourceType = .savedPhotosAlbum
        picker.mediaTypes = ["public.movie"]
        picker.allowsEditing = false
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func camera(_ sender: UIButton) {
        guard let pvCaptureVC = PhotoVideoCaptureDefaultBuilder().buildPhotoVideoCaptureViewController(
            shouldDisableAudioRecord: true,
            shouldStartWithFrontCamera: false,
            shouldDisableVideoRecord: false,
            delegate: self
            ) else { return }

        pvCaptureVC.modalPresentationStyle = .fullScreen
        self.present(pvCaptureVC, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let videoURL = info[UIImagePickerController.InfoKey.mediaURL] ?? info[UIImagePickerController.InfoKey.referenceURL]  as? URL {
            // Do something with the URL
            print("videoURL:\(String(describing: videoURL))")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension ViewController: PhotoVideoCaptureDelegate {
    func doneCaptureFile(_ sender: Any?, file: Any, image: UIImage?) {
        if let image = file as? UIImage {
            print(image)
            return
        }

        if let video = file as? URL {
            print(video)
            return
        }
    }
    
}


