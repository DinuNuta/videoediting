//
//  PhotoVideoCapturePresenter.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 21.09.2021.
//

import Foundation
import AVFoundation
import UIKit

enum CaptureType {
    case front
    case back
    case audio
}

protocol PhotoVideoCapturePresenter {

    func initializeSessionLayer(with captureType: CaptureType, and bounds: CGRect) -> AVCaptureVideoPreviewLayer?
    func capturePhoto()
    func startCapturingVideo()
    func stopCapturingVideo()
    func openGallery()
    func openAudioRecordingScreen()

    var isFlashEnabled: Bool { get set }
    var isBackCamera: Bool { get set }
}

class PhotoVideoCapturePresenterImp: PhotoVideoCapturePresenter {
   
    weak var view: PhotoVideoCaptureView!
    var router: PhotoVideoCaptureRouter!

    var isFlashEnabled: Bool = false
    var isBackCamera: Bool = true
    var photoOutput: AVCapturePhotoOutput?
    var videoOutput: AVCaptureMovieFileOutput?
    var videoFragmentTimeSize = 30.0
    var tempFilePath: URL = {
        let tempPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("fanatico_video.mp4")
        if FileManager.default.fileExists(atPath: tempPath.absoluteString) {
            do {
                try FileManager.default.removeItem(atPath: tempPath.absoluteString)
            } catch { }
        }
        return tempPath
    }()

    private lazy var cameraCaptureOutput = CameraCaptureOutput(parent: self)

    init(view: PhotoVideoCaptureView,
         router: PhotoVideoCaptureRouter) {

        self.view = view
        self.router = router
    }

    func initializeSessionLayer(with captureType: CaptureType,
                                and bounds: CGRect) -> AVCaptureVideoPreviewLayer? {
        let session = AVCaptureSession()
        session.sessionPreset = AVCaptureSession.Preset.hd1280x720
        if let device = micDevice() {
            session.addInput(device)
        }
        var camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
        switch captureType {
        case .back:
            camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
        case .front:
            camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
        case .audio:
            camera = AVCaptureDevice.default(.builtInMicrophone, for: AVMediaType.audio, position: .unspecified)
        }
        guard let selectedCamera = camera else {
            return nil
        }
        var input: AVCaptureDeviceInput?
        do {
            input = try AVCaptureDeviceInput(device: selectedCamera)
        } catch let error as NSError {
            input = nil
            print(error)
            return nil
        }
        guard let captureDeviceInput = input else {
            return nil
        }
        if session.canAddInput(captureDeviceInput) {
            session.addInput(captureDeviceInput)
            photoOutput = AVCapturePhotoOutput()
            videoOutput = AVCaptureMovieFileOutput()
            videoOutput?.movieFragmentInterval = CMTime.init(seconds: videoFragmentTimeSize, preferredTimescale: .max)
            
            photoOutput?.maxPhotoQualityPrioritization = .quality
            photoOutput?.isHighResolutionCaptureEnabled = true
            
            if session.canAddOutput(photoOutput!)
                && session.canAddOutput(videoOutput!) {

                session.addOutput(photoOutput!)
                session.addOutput(videoOutput!)
                let videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                videoPreviewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                session.startRunning()
                configMorroring(isFrontCamera: !isBackCamera)
                videoPreviewLayer.frame = bounds
                return videoPreviewLayer
            }
        }
        return nil
    }
    func configMorroring(isFrontCamera: Bool) {

        if let videoConnection = videoOutput?.connection(with: .video),
            videoConnection.isVideoMirroringSupported {
            videoConnection.isVideoMirrored = isFrontCamera
        }

        if let photoConnection = photoOutput?.connection(with: .video),
            photoConnection.isVideoMirroringSupported {
            photoConnection.isVideoMirrored = isFrontCamera
        }

    }
    func capturePhoto() {
        let photoSettings = AVCapturePhotoSettings()
        // Set photo settings for our need
        photoSettings.photoQualityPrioritization = .balanced
        photoSettings.isHighResolutionPhotoEnabled = true
        photoSettings.flashMode = isFlashEnabled ? .on : .off
        photoOutput?.capturePhoto(with: photoSettings, delegate: cameraCaptureOutput)
    }

    func startCapturingVideo() {
        videoOutput?.startRecording(to: tempFilePath, recordingDelegate: cameraCaptureOutput)
    }

    func stopCapturingVideo() {
        videoOutput?.stopRecording()
    }

    func openGallery() {
        router.showBuiltInGallery()
    }

    func openAudioRecordingScreen() {
        //TODO: add openAudioRecordingScreen
    }
    
    private func micDevice() -> AVCaptureDeviceInput? {
        guard let validDevice = AVCaptureDevice.default(for: AVMediaType.audio) else { return nil }
        do {
            return try AVCaptureDeviceInput(device: validDevice)
        } catch let outError {
            print(outError)
            return nil
        }
    }

    private class CameraCaptureOutput: NSObject,
        AVCapturePhotoCaptureDelegate,
    AVCaptureFileOutputRecordingDelegate {

        unowned let parent: PhotoVideoCapturePresenterImp

        init(parent: PhotoVideoCapturePresenterImp) {
            self.parent = parent
        }

        func fileOutput(_ output: AVCaptureFileOutput,
                        didFinishRecordingTo outputFileURL: URL,
                        from connections: [AVCaptureConnection],
                        error: Error?) {
            parent.router.showPreviewScreen(with: parent.tempFilePath)
        }

        func photoOutput(_ output: AVCapturePhotoOutput,
                         didFinishProcessingPhoto photo: AVCapturePhoto,
                         error: Error?) {
            guard let imageData = photo.fileDataRepresentation() ,
                let capturedImage = UIImage.init(data: imageData, scale: 1.0) else {return}
            parent.router.showPreviewScreen(with: capturedImage)
        }
    }
}
