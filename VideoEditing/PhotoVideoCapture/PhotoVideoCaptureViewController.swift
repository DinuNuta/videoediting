//
//  PhotoVideoCaptureViewController.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 21.09.2021.
//

import UIKit
import AVFoundation

protocol PhotoVideoCaptureView: AnyObject {

}

class PhotoVideoCaptureViewController: UIViewController, PhotoVideoCaptureView {

    var presenter: PhotoVideoCapturePresenter!
    var cameraTapInterval: Double = 0

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var actionsContainerView: UIStackView!
    @IBOutlet weak var micButton: UIButton!

    var timer: Timer?
    var timerSecondsCount = 0

    var viewDidSetup = false
    var shouldDisableAudioRecord = false
    var shouldStartWithFrontCamera = false
    var shouldDisableVideoRecord = false

    override func viewDidLoad() {
        super.viewDidLoad()

        if shouldDisableAudioRecord {
            micButton.isEnabled = false
            micButton.alpha = 0
        }

    }

    override func viewDidLayoutSubviews() {
        if !viewDidSetup {
            viewDidSetup = true
            setupOutlets()
        }
    }

    private func setupCaptureLayer(with captureType: CaptureType) {
        if let captureLayer = presenter.initializeSessionLayer(with: captureType,
                                                               and: self.view.bounds) {
            self.view.layer.sublayers?.forEach {
                if $0 is AVCaptureVideoPreviewLayer {
                    $0.removeFromSuperlayer()
                    return
                }
            }
            self.view.layer.insertSublayer(captureLayer, at: 0)
        }
    }

    private func setupOutlets() {
        if shouldStartWithFrontCamera { switchCameraOrientation() } else { setupCaptureLayer(with: .back) }

        bottomLabel.text = "camera_rec_info"
        flashButton.setImage(UIImage(named: "capture_off_flash"), for: .normal)

        bottomLabel.layer.shadowColor = UIColor.black.cgColor
        bottomLabel.layer.shadowRadius = 3.0
        bottomLabel.layer.shadowOpacity = 1.0
        bottomLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        bottomLabel.layer.masksToBounds = false

        timeLabel.layer.shadowColor = UIColor.black.cgColor
        timeLabel.layer.shadowRadius = 3.0
        timeLabel.layer.shadowOpacity = 1.0
        timeLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        timeLabel.layer.masksToBounds = false

        let rightRecognizer = UISwipeGestureRecognizer(target: self,
                                                       action: #selector(swipeMade(_:)))
        let downRecognizer = UISwipeGestureRecognizer(target: self,
                                                      action: #selector(swipeMade(_:)))
        rightRecognizer.direction = .right
        downRecognizer.direction = .down
        self.view.addGestureRecognizer(rightRecognizer)
        self.view.addGestureRecognizer(downRecognizer)
    }

    @IBAction func swipeMade(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .down {
            self.dismiss(animated: true, completion: nil)
        } else if sender.direction == .right {
            let transition: CATransition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            self.view.window!.layer.add(transition, forKey: nil)
            self.dismiss(animated: false, completion: nil)
        }

    }

    private func startLabelTimer() {
        timeLabel.isHidden = false
        timeLabel.text = "0:00"
        timerSecondsCount = 0
        timer = Timer.scheduledTimer(timeInterval: 1.0,
                                     target: self,
                                     selector: #selector(updateLabelTimer),
                                     userInfo: nil,
                                     repeats: true)
        timer?.fire()
    }

    @objc func updateLabelTimer() {
        let seconds: Int = timerSecondsCount % 60
        let minutes: Int = (timerSecondsCount / 60) % 60
        timeLabel.text = String(format: "%02d:%02d", minutes, seconds)
        timerSecondsCount += 1
    }

    private func stopLabelTimer() {
        timeLabel.isHidden = true
        timer?.invalidate()
    }

    private func removePulsingAnimation(for button: UIButton, tagList: [Int]) {
        tagList.forEach { button.viewWithTag($0)?.removeFromSuperview() }
    }

    private func addPulsingAnimation(for button: UIButton, tag: Int, scaleFrom: Double, scaleTo: Double) {
        let animationView = UIView(frame: CGRect(x: 0,
                                                 y: 0,
                                                 width: button.frame.width,
                                                 height: button.frame.height))
        animationView.cornerRadius = 11.0
        animationView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        animationView.layer.masksToBounds = true
        animationView.tag = tag
        button.insertSubview(animationView, at: 0)

        let pulseAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        pulseAnimation.duration = 1
        pulseAnimation.fromValue = 0
        pulseAnimation.toValue = 1
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = .greatestFiniteMagnitude

        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 1.0
        scaleAnimation.fromValue = scaleFrom
        scaleAnimation.toValue = scaleTo
        scaleAnimation.autoreverses = true
        scaleAnimation.repeatCount = .greatestFiniteMagnitude

        animationView.layer.add(scaleAnimation, forKey: "animateScale")
        animationView.layer.add(pulseAnimation, forKey: "animateOpacity")
    }

    private func switchCameraOrientation() {
        presenter.isBackCamera = !presenter.isBackCamera
        setupCaptureLayer(with: presenter.isBackCamera ? .back : .front)
    }
}

extension PhotoVideoCaptureViewController {

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func actionGallery(_ sender: Any) {
        presenter.openGallery()
    }

    @IBAction func actionMic(_ sender: Any) {
        presenter.openAudioRecordingScreen()
    }

    @IBAction func actionCameraTouchStarted(_ sender: Any) {
        guard let button = sender as? UIButton else {
            return
        }
        cameraTapInterval = Double(Date().timeIntervalSince1970)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if self.cameraTapInterval == 0 {
                return
            }
            self.removePulsingAnimation(for: button, tagList: [1212, 1213])
            self.addPulsingAnimation(for: button, tag: 1212, scaleFrom: 0.7, scaleTo: 1.0)
            self.addPulsingAnimation(for: button, tag: 1213, scaleFrom: 0.6, scaleTo: 1.2)
            self.startLabelTimer()
            self.presenter.startCapturingVideo()
        }
    }

    @IBAction func actionCamera(_ sender: Any) {
        guard let button = sender as? UIButton else { return }
        tapInterval() < 0.5 ? presenter.capturePhoto() : videoCapturing(button)
    }

    private func tapInterval() -> Double {
        let intervalTime = Double(Date().timeIntervalSince1970) - cameraTapInterval
        cameraTapInterval = 0
        return intervalTime
    }

    private func videoCapturing(_ button: UIButton) {
        if !shouldDisableVideoRecord {
            removePulsingAnimation(for: button, tagList: [1212, 1213])
            stopLabelTimer()
            presenter.stopCapturingVideo()
        }
    }

    @IBAction func actionFlash(_ sender: Any) {
        presenter.isFlashEnabled = !presenter.isFlashEnabled
        let imageName = presenter.isFlashEnabled ? "capture_on_flash" : "capture_off_flash"
        flashButton.setImage(UIImage(named: imageName), for: .normal)
    }

    @IBAction func actionRotate(_ sender: Any) {
        switchCameraOrientation()
    }
}

