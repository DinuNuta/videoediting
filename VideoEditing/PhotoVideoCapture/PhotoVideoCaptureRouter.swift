//
//  PhotoVideoCaptureRouter.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 21.09.2021.
//

import Foundation
import Photos
import UIKit

protocol PhotoVideoCaptureRouter {

    func showBuiltInGallery()
    
    func showPreviewScreen(with image: UIImage)
    func showPreviewScreen(with videoUrl: URL)

}

class PhotoVideoCaptureRouterImp: PhotoVideoCaptureRouter {

    weak var viewController: PhotoVideoCaptureViewController!
    
    weak var delegate: PhotoVideoCaptureDelegate?
    var delegateSender: Any?

    private lazy var imagePickerOutput = ImagePickerOutput(parent: self)

    func showBuiltInGallery() {
        //TODO add extracting CLLlocation
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = imagePickerOutput
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.mediaTypes = ["public.image", "public.movie"]
            imagePickerController.modalPresentationStyle = .fullScreen
            viewController.present(imagePickerController,
                                   animated: true,
                                   completion: nil)
        }
    }

    

    func showPreviewScreen(with videoUrl: URL) {
        guard let previewVC = PreviewDefaultBuilder().buildPreviewViewController(audioVideoUrl: videoUrl,
                                                                                 image: nil,
                                                                                 delegate: delegate,
                                                                                 sender: delegateSender) else { return }
        previewVC.modalPresentationStyle = .fullScreen
        viewController.present(previewVC, animated: true, completion: nil)
    }

    func showPreviewScreen(with image: UIImage) {
        guard let previewVC = PreviewDefaultBuilder().buildPreviewViewController(image: image,
                                                                                 delegate: delegate,
                                                                                 sender: delegateSender) else { return }
        previewVC.modalPresentationStyle = .fullScreen
        viewController.present(previewVC, animated: true, completion: nil)
    }
    
    private class ImagePickerOutput: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

        unowned let parent: PhotoVideoCaptureRouterImp

        init(parent: PhotoVideoCaptureRouterImp) {
            self.parent = parent
        }

        func imagePickerController(_ picker: UIImagePickerController,
                                   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
            picker.dismiss(animated: true, completion: {
                if let img = info[.originalImage] as? UIImage {
                    self.parent.showPreviewScreen(with: img)
                } else if let videoUrl = info[.mediaURL] as? URL {
                    self.parent.showPreviewScreen(with: videoUrl)
                }
            })
        }
    }
}
