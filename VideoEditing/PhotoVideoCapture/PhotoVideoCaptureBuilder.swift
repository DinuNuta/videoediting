//
//  PhotoVideoCaptureBuilder.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 21.09.2021.
//

import Foundation
import UIKit
import Swinject

protocol PhotoVideoCaptureDelegate: AnyObject {
    func doneCaptureFile(_ sender: Any?, file: Any, image: UIImage?)
}

protocol PhotoVideoCaptureBuilder {
    func buildPhotoVideoCaptureViewController(shouldDisableAudioRecord: Bool,
                                              shouldStartWithFrontCamera: Bool,
                                              shouldDisableVideoRecord: Bool,
                                              delegate: PhotoVideoCaptureDelegate?,
                                              sender: Any?) -> PhotoVideoCaptureViewController!
}

class PhotoVideoCaptureDefaultBuilder: PhotoVideoCaptureBuilder {

    let container = Container()

    func buildPhotoVideoCaptureViewController(shouldDisableAudioRecord: Bool,
                                              shouldStartWithFrontCamera: Bool,
                                              shouldDisableVideoRecord: Bool,
                                              delegate: PhotoVideoCaptureDelegate? = nil,
                                              sender: Any? = nil) -> PhotoVideoCaptureViewController! {

        container.register(PhotoVideoCaptureViewController.self) { _ in
            let nibName = String(describing: PhotoVideoCaptureViewController.self)
            let viewController = PhotoVideoCaptureViewController(nibName: nibName, bundle: Bundle.main)
            viewController.shouldDisableAudioRecord = shouldDisableAudioRecord
            viewController.shouldStartWithFrontCamera = shouldStartWithFrontCamera
            viewController.shouldDisableVideoRecord = shouldDisableVideoRecord
            return viewController
        }.initCompleted { r, h in
            h.presenter = r.resolve(PhotoVideoCapturePresenter.self)
        }

        container.register(PhotoVideoCaptureRouter.self) { c in
            let router = PhotoVideoCaptureRouterImp()
            router.viewController = c.resolve(PhotoVideoCaptureViewController.self)!
            router.delegate = delegate
            router.delegateSender = sender
            return router
        }

        container.register(PhotoVideoCapturePresenter.self) { c in
            PhotoVideoCapturePresenterImp(view: c.resolve(PhotoVideoCaptureViewController.self)!,
                                          router: c.resolve(PhotoVideoCaptureRouter.self)!)
        }

        return container.resolve(PhotoVideoCaptureViewController.self)!
    }

    deinit {
        container.removeAll()
    }
}
