//
//  TextOverlayManipulationView.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

protocol TextOverlayManipulationViewDelegate: AnyObject {
    var textOverlayDidLoad: Bool { get }

    func updateManipulationViewAlignment()
    func updateBackgroundColorComponents()
    func didSelectAlignmentButton(_ sender: UIButton)
    func didSelectBackgroundColorButton(_ sender: UIButton)
}

protocol TextOverlayManipulationView: AnyObject {
    var alignmentButton: UIButton? { get set }
    var backgroundColorButton: UIButton? { get set }

    func moveHorizontally(by adjustment: CGFloat)
    func hideComponents(_ shouldHide: Bool)
}

class TextOverlayManipulationViewImp: UIStackView, TextOverlayManipulationView {

    unowned var parentView: UIView!
    weak var delegate: TextOverlayManipulationViewDelegate?

    var objectsCount = 2

    var alignmentButton: UIButton?
    var backgroundColorButton: UIButton?

    var topConstraint: NSLayoutConstraint?

    init(parentView: UIView,
         delegate: TextOverlayManipulationViewDelegate,
         shouldShowAlignment: Bool,
         shouldShowBackgroundColor: Bool,
         shouldShowFontStyle: Bool) {

        self.parentView = parentView
        self.delegate = delegate

        super.init(frame: .zero)

        customInit()

        if shouldShowAlignment { setupAlignmentButton() }
        if shouldShowBackgroundColor { setupBackgroundColorButton() }
        if shouldShowFontStyle { setupFontStyleButton() }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func customInit() {
        parentView.addSubview(self)

        translatesAutoresizingMaskIntoConstraints = false

        topConstraint = topAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.topAnchor, constant: constants.textOverlay_topViewTopArchon)

        let widthConstant = constants.textOverlay_manipulationViewWidth * CGFloat(objectsCount)

        [topConstraint,
         leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: constants.textOverlay_topViewLeftArchon),
         widthAnchor.constraint(equalToConstant: widthConstant),
         heightAnchor.constraint(equalToConstant: constants.textOverlay_topViewMeasurement)
            ].forEach { $0?.isActive = true }

        axis = .horizontal
        distribution = .equalSpacing
        alignment = .center
        spacing = 10
    }

    private func setupAlignmentButton() {
        delegate?.updateManipulationViewAlignment()
        if delegate?.textOverlayDidLoad ?? true { return }

        alignmentButton = UIButton()
        guard let alignmentButton = alignmentButton else { return }

        alignmentButton.setImage(UIImage(asset: .centerAlignment), for: .normal)

        [alignmentButton.widthAnchor.constraint(equalToConstant: constants.textOverlay_topViewMeasurement),
         alignmentButton.heightAnchor.constraint(equalToConstant: constants.textOverlay_topViewMeasurement)
            ].forEach { $0.isActive = true }

        alignmentButton.addTarget(self, action: #selector(didSelectAlignmentButton(_:)), for: .touchUpInside)

        addArrangedSubview(alignmentButton)
    }

    @objc
    private func didSelectAlignmentButton(_ sender: UIButton) {
        delegate?.didSelectAlignmentButton(sender)
    }

    private func setupBackgroundColorButton() {
        delegate?.updateBackgroundColorComponents()
        if delegate?.textOverlayDidLoad ?? true { return }

        backgroundColorButton = UIButton()
        guard let backgroundColorButton = backgroundColorButton else { return }

        backgroundColorButton.setImage(UIImage(asset: .a1), for: .normal)

        [backgroundColorButton.widthAnchor.constraint(equalToConstant: constants.textOverlay_topViewMeasurement),
         backgroundColorButton.heightAnchor.constraint(equalToConstant: constants.textOverlay_topViewMeasurement)
            ].forEach { $0.isActive = true }

        backgroundColorButton.addTarget(self, action: #selector(didSelectBackgroundColorButton(_:)), for: .touchUpInside)

        addArrangedSubview(backgroundColorButton)
    }

    @objc
    private func didSelectBackgroundColorButton(_ sender: UIButton) {
        delegate?.didSelectBackgroundColorButton(sender)
    }

    func hideComponents(_ shouldHide: Bool) {
        if shouldHide {
            alignmentButton?.alpha = 0
            backgroundColorButton?.alpha = 0
        } else {
            alignmentButton?.alpha = 1
            backgroundColorButton?.alpha = 1
        }
    }

    func moveHorizontally(by adjustment: CGFloat) {
        topConstraint?.constant += adjustment
    }

    private func setupFontStyleButton() {
        // TODO
    }

    
    //  func setupFontStyleButton() {
    //    standardTextFont()
    //    if textOverlayDidLoad { return }
    //
    //    textOverlayTextFont = UIButton()
    //    guard let textOverlayTextFont = textOverlayTextFont else { return }
    //    guard let backgroundColorButton = backgroundColorButton else { return }
    //
    //    toolViewControllerDelegate.view.addSubview(textOverlayTextFont)
    //
    //    textOverlayTextFont.translatesAutoresizingMaskIntoConstraints = false
    //
    //    textFontTopAnchor = textOverlayTextFont.topAnchor.constraint(equalTo: toolViewControllerDelegate.view.safeAreaLayoutGuide.topAnchor,
    //                                                                            constant: constants.textOverlay_topViewTopArchon)
    //
    //    [textFontTopAnchor,
    //     textOverlayTextFont.leftAnchor.constraint(equalTo: backgroundColorButton.rightAnchor, constant: constants.textOverlay_topViewLeftArchon),
    //     textOverlayTextFont.widthAnchor.constraint(equalToConstant: constants.textOverlay_topViewMeasurement * 2),
    //     textOverlayTextFont.heightAnchor.constraint(equalToConstant: constants.textOverlay_topViewMeasurement)
    //      ].forEach { $0?.isActive = true }
    //
    //    textOverlayTextFont.backgroundColor = .darkGray
    //    textOverlayTextFont.setTitle("C", for: .normal) // TODO: Change title to icon
    //    textOverlayTextFont.addTarget(self, action: #selector(didSelectFontButton(_:)), for: .touchUpInside)
    //
    //    textOverlayTextFont.isHidden = true
    //    textOverlayTextFont.isEnabled = false
    //  }
}
