//
//  TextOverlayColors.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

protocol ColorPaletteDelegate: class {
    var selectedColor: UIColor {get}
    func didSet(color: UIColor)
}

class ColorPaletteImp: UICollectionView {
    weak var toolViewControllerDelegate: (ToolViewDelegate & UIViewController)?
    weak var colorDelegate: ColorPaletteDelegate?

    init(toolViewControllerDelegate: ToolViewDelegate & UIViewController,
         delegate: ColorPaletteDelegate?) {

        self.toolViewControllerDelegate = toolViewControllerDelegate
        self.colorDelegate = delegate

        let frame = CGRect(x: 0,
                           y: toolViewControllerDelegate.view.frame.size.height,
                           width: toolViewControllerDelegate.view.frame.size.width,
                           height: constants.textOverlay_colorsHeight)

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        super.init(frame: frame, collectionViewLayout: layout)

        customInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func customInit() {
        registerNibForCell(TextOverlayColorsCell.self)
        delegate = self
        dataSource = self
        backgroundColor = .clear
        tag = constants.textOverlay_colorsTag
        toolViewControllerDelegate?.view.addSubview(self)
        setupKeyboardNotifications()
    }
    var keyboardHeight: CGFloat = UIScreen.main.bounds.size.height

    func shouldShow(show: Bool) {
        DispatchQueue.main.async {
            let size: CGSize = self.toolViewControllerDelegate?.view.frame.size ?? UIScreen.main.bounds.size
            let y = show ? min(self.keyboardHeight, size.height - constants.textOverlay_colorsHeight) : size.height
            let frame = CGRect(x: 0, y: y,
                               width: size.width,
                               height: constants.textOverlay_colorsHeight)

            UIView.animate(withDuration: constants.textOverlay_basicAnimationDuration,
                           delay: constants.textOverlay_basicAnimationDelay,
                           options: .curveEaseIn,
                           animations: {
                            self.frame = frame
                            self.layoutIfNeeded()
            }, completion: nil)
        }
    }

    func resetPalette() {
        //        self.colorDelegate?.selectedColor // use this for evidentiate of current item color
        self.reloadData()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension ColorPaletteImp: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return constants.textOverlay_colorsStates
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = String(describing: TextOverlayColorsCell.self)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! TextOverlayColorsCell

        cell.selectedClouser = {[weak self] (color) in
            self?.colorDelegate?.didSet(color: color)
        }
        cell.row = indexPath.row
        cell.setupColorCell()

        return cell
    }
}

extension ColorPaletteImp: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: constants.textOverlay_colorsItemMeasurement, height: constants.textOverlay_colorsItemMeasurement)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: constants.textOverlay_colorsItemInset, left: constants.textOverlay_colorsItemInset,
                            bottom: constants.textOverlay_colorsItemInset, right: constants.textOverlay_colorsItemInset)
    }
}

extension ColorPaletteImp {

    func setupKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboadWillChange),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboadWillChange),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        toolViewControllerDelegate?.hideKeyboardWhenFinishEditing()
    }

    @objc
    private func keyboadWillChange(_ notification: Notification) {
        guard let toolViewControllerDelegate = toolViewControllerDelegate else {return}
        guard let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        guard let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else { return }
        let parentView = toolViewControllerDelegate.view
        let keyboardViewEndFrame = parentView?.convert(keyboardFrame, from: parentView?.window) ?? keyboardFrame

        let yConst = notification.name == UIResponder.keyboardWillShowNotification
            ? keyboardViewEndFrame.origin.y - constants.textOverlay_colorsHeight
            : toolViewControllerDelegate.view.frame.size.height
        self.keyboardHeight = yConst
        self.frame.origin.y = yConst

        //        DispatchQueue.main.async {
        //            UIView.animate(withDuration: keyboardDuration,
        //                           delay: constants.textOverlay_basicAnimationDelay,
        //                           options: .curveEaseIn,
        //                           animations: {
        //                            self.frame.origin.y = _y
        //                            self.toolViewControllerDelegate?.view.layoutIfNeeded()
        //            }, completion: nil)
        //        }
    }
}
