//
//  TextOverlayColorsCell.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

class TextOverlayColorsCell: UICollectionViewCell {

    @IBOutlet weak var colorButton: UIButton!

    var row: Int?
    var color: UIColor?
    var selectedClouser: ((UIColor) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        colorButton.roundCorners(.all, radius: 10)
    }

    func setupColorCell() {
        guard let row = row else { return }
        switch row {
        case 0:
            colorButton.backgroundColor = .white
            color = .white
        case 1:
            colorButton.backgroundColor = .black
            color = .black
        case 2:
            colorButton.backgroundColor = .red
            color = .red
        case 3:
            colorButton.backgroundColor = .yellow
            color = .yellow
        case 4:
            colorButton.backgroundColor = .green
            color = .green
        case 5:
            colorButton.backgroundColor = .blue
            color = .blue
        case 6:
            colorButton.backgroundColor = .brown
            color = .brown
        case 7:
            colorButton.backgroundColor = .lightGray
            color = .lightGray
        case 8:
            colorButton.backgroundColor = .orange
            color = .orange
        case 9:
            colorButton.backgroundColor = .cyan
            color = .cyan
        case 10:
            colorButton.backgroundColor = .systemPink
            color = .systemPink
        case 11:
            colorButton.backgroundColor = .purple
            color = .purple
        case 12:
            colorButton.backgroundColor = .orange
            color = .orange
        default: break
        }

        colorButton.borderWidth = 1.0
        colorButton.borderColor = .white
    }

    @IBAction func didSelectColor(_ sender: UIButton) {
        guard let color = color else { return }
        selectedClouser?(color)
    }
}
