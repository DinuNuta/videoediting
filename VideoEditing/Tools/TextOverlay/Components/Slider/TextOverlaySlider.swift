//
//  TextOverlaySlider.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

protocol TextOverlaySliderDelegate: class {
    func didChangeSliderValue(_ sender: UISlider, event: UIEvent)
    func didTouchDownSlider(_ sender: UISlider)
    func didTouchUPSlider(_ sender: UISlider)
}
extension TextOverlaySliderDelegate {
    func didTouchDownSlider(_ sender: UISlider) {}
    func didTouchUPSlider(_ sender: UISlider) {}
}

class TextOverlaySliderImp: UISlider {

    unowned var parentView: UIView!
    weak var delegate: TextOverlaySliderDelegate?

    var leftConstraint: NSLayoutConstraint?

    init(parentView: UIView,
         delegate: TextOverlaySliderDelegate,
         minimumValue: Float,
         maximumValue: Float,
         startValue: Float) {

        self.parentView = parentView
        self.delegate = delegate

        super.init(frame: .zero)

        self.minimumValue = minimumValue
        self.maximumValue = maximumValue
        setValue(startValue, animated: false)

        customInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func customInit() {
        parentView.addSubview(self)

        translatesAutoresizingMaskIntoConstraints = false
        tag = 0
        transform = CGAffineTransform(rotationAngle: .pi/2*3)

        let leftAnchorConstant = -(constants.textOverlay_sliderWidthRotated / 2)
            + (constants.textOverlay_sliderHeightRotated / 2)
            - constants.textOverlay_adjustment

        leftConstraint = leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: leftAnchorConstant)

        [leftConstraint,
         widthAnchor.constraint(equalToConstant: constants.textOverlay_sliderWidthRotated),
         bottomAnchor.constraint(equalTo: parentView.centerYAnchor, constant: -50),
         heightAnchor.constraint(equalToConstant: constants.textOverlay_sliderHeightRotated)
            ].forEach { $0?.isActive = true }

        backgroundColor = .clear
        isEnabled = true
        alpha = 0
        isUserInteractionEnabled = true
        roundCorners(.all, radius: constants.textOverlay_cornerRadius)

        addTarget(self, action: #selector(didChangeSliderValue(_:event:)), for: .valueChanged)
        addTarget(self, action: #selector(didTouchDownSlider(_:)), for: .touchDown)
        addTarget(self, action: #selector(didTouchUPSlider(_:)), for: .touchUpInside)
        addTarget(self, action: #selector(didTouchUPSlider(_:)), for: .touchUpOutside)
        addTarget(self, action: #selector(didTouchUPSlider(_:)), for: .touchCancel)
    }

    public func moveHorizontally(by adjustment: CGFloat) {
        leftConstraint?.constant += adjustment
    }

    @objc
    private func didChangeSliderValue(_ sender: UISlider, event: UIEvent) {
        delegate?.didChangeSliderValue(sender, event: event)
    }

    @objc
    private func didTouchDownSlider(_ sender: UISlider) {
        delegate?.didTouchDownSlider(sender)
    }
    @objc
    private func didTouchUPSlider(_ sender: UISlider) {
        delegate?.didTouchUPSlider(sender)
    }
}
