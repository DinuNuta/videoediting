//
//  TextOverlay.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

class TextOverlay: NSObject, UIGestureRecognizerDelegate, TextOverlayManipulationViewDelegate {

    //temporary unowned normaly should be weak
    unowned var toolViewControllerDelegate: (ToolViewDelegate & UIViewController)!
    var undoManipulationManager: UndoManipulationManager

    var textOverlays: [UITextView] = []
    var textOverlaysPosition: [Int: CGPoint] = [:]
    var removedTextOverlay: UITextView?

    var manipulationView: TextOverlayManipulationView?
    var slider: TextOverlaySliderImp?
    var alignmentState = 0
    var darkBackground: UIView?
    var textOverlayBackgroundState = 0
    var textOverlayTextFont: UIButton?
    var centerXLine: UIView?
    var tag: Int?

    var xAxisAnchors: [NSLayoutConstraint] = []
    var yAxisAnchors: [NSLayoutConstraint] = []

    var shouldRemoveCenteredConstraints = false
    var isEditing = false
    var textOverlayDidLoad = false

    var panGesture: UIPanGestureRecognizer?
    var tapGesture: UITapGestureRecognizer?

    var animator: UIDynamicAnimator?
    var snapping: UISnapBehavior?

    init(toolViewControllerDelegate: ToolViewDelegate & UIViewController,
         undoManipulationManager: UndoManipulationManager) {

        self.toolViewControllerDelegate = toolViewControllerDelegate
        self.undoManipulationManager = undoManipulationManager
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        setupDarkBackgroundView()
        setupManipulationView()
        setupSlider()
        self.toolViewControllerDelegate.subscribeToColorPalete(item: self)
        setupTextOverlay()

        guard let textOverlay = textOverlay(by: textOverlays.count - 1) else { return }

        adjustTextOverlaySize(tag: textOverlay.tag)

        setupTapGestureIfNeeded(for: textOverlay)
        setupPanGestureIfNeeded(for: textOverlay)

        textOverlayDidLoad = true
    }

    func remove() {
        self.toolViewControllerDelegate.subscribeToColorPalete(item: nil)
        toolViewControllerDelegate.view?.endEditing(true)
    }

    func removeLastTextOverlay() {
        guard let textOverlay = textOverlays.last else { return }
        textOverlay.removeFromSuperview()
        removeTextOverlayGestures(from: textOverlay)
        textOverlays.removeLast()
        textOverlaysPosition.removeValue(forKey: textOverlay.tag)

        if textOverlays.isEmpty { clearTextOverlay() }
    }

    func removeTextOverlay(by tag: Int) {
        guard let textOverlay = textOverlay(by: tag) else { return }
        textOverlay.removeFromSuperview()
        removeTextOverlayGestures(from: textOverlay)
        textOverlays.remove(at: tag)
        textOverlaysPosition.removeValue(forKey: textOverlay.tag)

        if textOverlays.isEmpty { clearTextOverlay() }
    }

    private func clearTextOverlay() {
        toolViewControllerDelegate.didAddTextOverlay = false
        toolViewControllerDelegate.view.subviews.filter({ $0.tag == constants.textOverlay_darkBackgroundTag }).first?.removeFromSuperview()
    }

    func textOverlay(by tag: Int) -> UITextView? {
        return textOverlays.filter { $0.tag == tag }.first
    }

    private func removeTextOverlayGestures(from textOverlay: UITextView, selectedGestures: [UIGestureRecognizer]? = nil) {
        selectedGestures != nil
            ? selectedGestures?.forEach { textOverlay.removeGestureRecognizer($0) }
            : textOverlay.gestureRecognizers?.forEach { textOverlay.removeGestureRecognizer($0) }
    }

    func changeTextColor(for tag: Int, selectedColor: UIColor) {
        guard let textOverlay = textOverlay(by: tag) else { return }
        textOverlay.textColor = selectedColor
    }

    func shouldHideTextOverlayTools(_ shouldHide: Bool = true) {

        let adjustment: CGFloat = shouldHide ? -constants.textOverlay_adjustment : constants.textOverlay_adjustment

        slider?.moveHorizontally(by: adjustment)
        manipulationView?.moveHorizontally(by: adjustment)

        UIView.animate(withDuration: constants.textOverlay_basicAnimationDuration,
                       delay: constants.textOverlay_basicAnimationDelay,
                       options: .curveEaseIn,
                       animations: {

                        self.slider?.alpha = 1
                        self.manipulationView?.hideComponents(false)
                        self.toolViewControllerDelegate.view.layoutIfNeeded()
                        self.toolViewControllerDelegate.colorPalette.flatMap({self.toolViewControllerDelegate.view.bringSubviewToFront($0)})

        }, completion: nil)
    }

    func addTextOverlay(_ textOverlay: UITextView, isUndoAvailable: Bool) {
        toolViewControllerDelegate.view.addSubview(textOverlay)
        if isUndoAvailable { self.undoManipulationManager.append(manipulation: .added(component: .textOverlay(tag: textOverlay.tag))) }
    }

    func updateManipulationViewAlignment() {
        updateAlignmentComponents(for: textOverlays.count, state: .center)
    }

    func updateBackgroundColorComponents() {
        updateBackgroundColorComponents(for: textOverlays.count, backgroundColor: .clear)
    }
}

extension TextOverlay: ToolTriggerDelegate {

    func isActive() -> Bool {
        return false
    }

    func activate(_ shouldActivate: Bool) {
        shouldActivate ? setup() : remove()
    }

    func disable(_ disable: Bool) {
        DispatchQueue.main.async {
            self.textOverlays.forEach {($0.isUserInteractionEnabled = !disable)}
        }
    }
}
