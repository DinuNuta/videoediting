//
//  TextOverlayGesturesHandler.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

extension TextOverlay: TextOverlaySliderDelegate {

    func didChangeSliderValue(_ sender: UISlider, event: UIEvent) {
        removeSnapping()
        updateFontComponents(for: sender.tag, fontSize: CGFloat(sender.value))

        if let touchEvent = event.allTouches?.first, touchEvent.phase == .began {
            undoManipulationManager.append(manipulation: .changedFontSize(component: .textOverlay(tag: sender.tag),
                                                                          fontSize: CGFloat(sender.value)))
        }
    }

    func updateFontComponents(for tag: Int, fontSize: CGFloat) {
        guard let textOverlay = textOverlay(by: tag) else { return }
        textOverlay.font = UIFont.systemFont(ofSize: fontSize, weight: .regular)
        slider?.value = Float(fontSize)
        adjustTextOverlaySize(tag: tag)
    }

    func didSelectAlignmentButton(_ sender: UIButton) {
        alignmentState = alignmentState <= constants.textOverlay_alignmentStates ? alignmentState + 1 : 0

        guard let textOverlay = textOverlay(by: sender.tag) else { return }
        guard let alignment = manipulationView?.alignmentButton else { return }

        switch alignmentState {
        case 0:
            textOverlay.textAlignment = .center
            alignment.setImage(UIImage(asset: .centerAlignment), for: .normal)
        case 1:
            textOverlay.textAlignment = .left
            alignment.setImage(UIImage(asset: .leftAlignment), for: .normal)
        case 2:
            textOverlay.textAlignment = .right
            alignment.setImage(UIImage(asset: .rightAlignment), for: .normal)
        case 3:
            textOverlay.textAlignment = .justified
            alignment.setImage(UIImage(asset: .centerAlignment), for: .normal)
        default: break
        }

        undoManipulationManager.append(manipulation: .changedAlignment(component: .textOverlay(tag: sender.tag),
                                                                       alignment: textOverlay.textAlignment))
    }

    func updateAlignmentComponents(for tag: Int, state: NSTextAlignment) {

        guard let alignment = manipulationView?.alignmentButton else { return }
        alignment.tag = tag

        switch state {
        case .center:
            alignmentState = 0
            alignment.setImage(UIImage(asset: .centerAlignment), for: .normal)
        case .left:
            alignmentState = 1
            alignment.setImage(UIImage(asset: .leftAlignment), for: .normal)
        case .right:
            alignmentState = 2
            alignment.setImage(UIImage(asset: .rightAlignment), for: .normal)
        case .justified:
            alignmentState = 3
            alignment.setImage(UIImage(asset: .centerAlignment), for: .normal)
        default: break
        }

        guard let textOverlay = textOverlay(by: tag) else { return }
        textOverlay.textAlignment = state
    }

    func didSelectBackgroundColorButton(_ sender: UIButton) {
        textOverlayBackgroundState = textOverlayBackgroundState <= constants.textOverlay_backgroundStates ? textOverlayBackgroundState + 1 : 0

        guard let textOverlay = textOverlay(by: sender.tag) else { return }
        guard let backgroundColorButton = manipulationView?.backgroundColorButton else { return }
        guard let backgroundColor = textOverlay.backgroundColor else { return }

        switch textOverlayBackgroundState {
        case 0:
            textOverlay.backgroundColor = .clear
            backgroundColorButton.setImage(UIImage(asset: .a1), for: .normal)
        case 1:
            textOverlay.backgroundColor = .lightGray
            backgroundColorButton.setImage(UIImage(asset: .a2), for: .normal)
        case 2:
            textOverlay.backgroundColor = .white
            backgroundColorButton.setImage(UIImage(asset: .a3), for: .normal)
        case 3:
            textOverlay.backgroundColor = .black
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.black), for: .normal)
        case 4:
            textOverlay.backgroundColor = .red
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.red), for: .normal)
        case 5:
            textOverlay.backgroundColor = .yellow
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.yellow), for: .normal)
        case 6:
            textOverlay.backgroundColor = .green
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.green), for: .normal)
        case 7:
            textOverlay.backgroundColor = .blue
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.blue), for: .normal)
        case 8:
            textOverlay.backgroundColor = .brown
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.brown), for: .normal)
        case 9:
            textOverlay.backgroundColor = .orange
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.orange), for: .normal)
        case 10:
            textOverlay.backgroundColor = .cyan
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.cyan), for: .normal)
        case 11:
            textOverlay.backgroundColor = .systemPink
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.systemPink), for: .normal)
        case 12:
            textOverlay.backgroundColor = .purple
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.purple), for: .normal)
        case 13:
            textOverlay.backgroundColor = .orange
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.orange), for: .normal)
        default: break
        }

        undoManipulationManager.append(manipulation: .changedTextOverlayBackgroundColor(component: .textOverlay(tag: sender.tag),
                                                                                        color: backgroundColor))
    }

    func updateBackgroundColorComponents(for tag: Int, backgroundColor: UIColor) {

        guard let backgroundColorButton = manipulationView?.backgroundColorButton else { return }
        backgroundColorButton.tag = tag

        switch backgroundColor {
        case .clear:
            textOverlayBackgroundState = 0
            backgroundColorButton.setImage(UIImage(asset: .a1), for: .normal)
        case .lightGray:
            textOverlayBackgroundState = 1
            backgroundColorButton.setImage(UIImage(asset: .a2), for: .normal)
        case .white:
            textOverlayBackgroundState = 2
            backgroundColorButton.setImage(UIImage(asset: .a3), for: .normal)
        case .black:
            textOverlayBackgroundState = 3
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor(.black), for: .normal)
        case .red:
            textOverlayBackgroundState = 4
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .red), for: .normal)
        case .yellow:
            textOverlayBackgroundState = 5
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .yellow), for: .normal)
        case .green:
            textOverlayBackgroundState = 6
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .green), for: .normal)
        case .blue:
            textOverlayBackgroundState = 7
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .blue), for: .normal)
        case .brown:
            textOverlayBackgroundState = 8
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .brown), for: .normal)
        case .orange:
            textOverlayBackgroundState = 9
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .orange), for: .normal)
        case .cyan:
            textOverlayBackgroundState = 10
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .cyan), for: .normal)
        case .systemPink:
            textOverlayBackgroundState = 11
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .systemPink), for: .normal)
        case .purple:
            textOverlayBackgroundState = 12
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .purple), for: .normal)
        case .orange:
            textOverlayBackgroundState = 13
            backgroundColorButton.setImage(UIImage(asset: .a2).withTintColor( .orange), for: .normal)
        default: break
        }

        guard let textOverlay = textOverlay(by: tag) else { return }
        textOverlay.backgroundColor = backgroundColor
    }

    @objc
    func didSelectFontButton(_ sender: UIButton) {
        // TODO
    }

    func updateFontStyleComponents(for tag: Int, font: UIFont) {
        // TODO
    }

    @objc
    func handleTapGesture(_ sender: UITapGestureRecognizer) {
        guard let senderView = sender.view else { return }
        let textOverlay = self.textOverlay(by: senderView.tag)

        let location = sender.location(in: textOverlay)
        guard let tapLocation = textOverlay?.closestPosition(to: location) else { return }
        guard let textRange = textOverlay?.tokenizer.rangeEnclosingPosition(tapLocation,
                                                                            with: .word,
                                                                            inDirection: .layout(.right)) else { return }
        //    let word = textOverlay?.text(in: textRange)

        textOverlay?.selectedTextRange = textOverlay?.textRange(from: textRange.end, to: textRange.end)

        if !isEditing { textOverlay?.becomeFirstResponder() }
    }

    @objc
    func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        if !shouldRemoveCenteredConstraints { shouldRemoveCenteredConstraints = true }
        let translation = sender.translation(in: toolViewControllerDelegate.view)

        guard let tag = sender.view?.tag else { return }
        guard let textOverlay = textOverlay(by: tag) else { return }

        textOverlay.center = CGPoint(x: textOverlay.center.x + translation.x, y: textOverlay.center.y + translation.y)
        sender.setTranslation(.zero, in: toolViewControllerDelegate.view)

        let centerX = toolViewControllerDelegate.view.center.x
        let magnetRange = centerX - constants.textOverlay_centerXAdjustment ... centerX + constants.textOverlay_centerXAdjustment

        switch sender.state {
        case .began:
            toolViewControllerDelegate.shouldHidePreviewTools(true)
            if let snapping = snapping { animator?.removeBehavior(snapping) }
            undoManipulationManager.append(manipulation: .changedPosition(component: .textOverlay(tag: tag), center: textOverlay.center))
        case .ended:
            toolViewControllerDelegate.shouldHidePreviewTools(false)
            hideCenterXLine()
            if magnetRange ~= textOverlay.center.x { setupSnapping(for: tag) }
            adjustTextOverlaySize(tag: tag)
        case .changed:
            xAxisAnchors[tag].constant += translation.x
            yAxisAnchors[tag].constant += translation.y
            magnetRange ~= textOverlay.center.x ? setupCenterXLine() : hideCenterXLine()
            textOverlaysPosition[tag] = CGPoint(x: xAxisAnchors[tag].constant, y: yAxisAnchors[tag].constant)
        default: break
        }
    }
}
