//
//  TextOverlayTextViewDelegate.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

extension TextOverlay: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        isEditing = true
        removeSnapping()

        updateComponents(for: textView.tag)
        shouldShowDarkBackground(true, for: textView.tag)

        shouldHideTextOverlayTools(false)
        toolViewControllerDelegate.shouldHidePreviewTools(true)
        toolViewControllerDelegate.deactivateUnusedToolsWhileUsingTextOverlay()
        relocate(true, selectedTextViewTag: textView.tag)
        toolViewControllerDelegate.subscribeToColorPalete(item: self)
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        isEditing = false

        if textView.text.isEmpty { removeTextOverlay(by: textView.tag) }
        shouldShowDarkBackground(false, for: textView.tag)

        shouldHideTextOverlayTools(true)
        toolViewControllerDelegate.shouldHidePreviewTools(false)
        toolViewControllerDelegate.deactivateTextOverlay(delegate: self)

        relocate(false, selectedTextViewTag: textView.tag)
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let updatedText = (textView.text as NSString).replacingCharacters(in: range, with: text)

        if updatedText.count % constants.textOverlay_symbolsTargetCount == 0
            && textView.text.count < updatedText.count {
            undoManipulationManager.append(manipulation: .changedValue(component: .textOverlay(tag: textView.tag), value: updatedText))
        } else if updatedText.count % constants.textOverlay_symbolsTargetCount == 0
            && textView.text.count > updatedText.count && !updatedText.isEmpty {
            undoManipulationManager.removeManipulationFrom(undoComponent: .textOverlay(tag: textView.tag))
        }

        return true
    }

    func textViewDidChange(_ textView: UITextView) {
        adjustTextOverlaySize(tag: textView.tag)
    }

    func adjustTextOverlaySize(tag: Int) {
        guard let textOverlay = textOverlay(by: tag) else { return }
        let maximumWidth = toolViewControllerDelegate.view.frame.size.width - constants.textOverlay_sideInsets
        let size = CGSize(width: maximumWidth, height: .infinity)
        let estimatedSize = textOverlay.sizeThatFits(size)
        let optimalSize = CGSize(width: max(constants.textOverlay_initialWidth, estimatedSize.width), height: estimatedSize.height)

        textOverlay.constraints.forEach {
            switch $0.firstAttribute {
            case .height: $0.constant = optimalSize.height
            case .width: $0.constant = optimalSize.width
            default: break
            }
        }

        if shouldRemoveCenteredConstraints {
            let xConstant = textOverlay.center.x - toolViewControllerDelegate.view.center.x
            let yConstant = textOverlay.center.y - toolViewControllerDelegate.view.center.y

            toolViewControllerDelegate.view.constraints.forEach {
                if $0 == xAxisAnchors[tag] { $0.constant = xConstant } else if $0 == yAxisAnchors[tag] { $0.constant = yConstant }
            }
            shouldRemoveCenteredConstraints = false
        }
    }
}
