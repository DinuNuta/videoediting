//
//  TextOverlayUndoExtension.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

extension TextOverlay {

    func undoAdd() {
        removeLastTextOverlay()
    }

    func undoRemove() {
        guard let textOverlay = removedTextOverlay else { return }
        addTextOverlay(textOverlay, isUndoAvailable: false)
        toolViewControllerDelegate.triggerToolIndicator(tool: .text, delegate: self)
    }

    func undoChangePosition(tag: Int, center: CGPoint) {
        guard let textOverlay = textOverlay(by: tag) else { return }
        textOverlay.center = center
    }

    func undo(tag: Int, fontSize: CGFloat) {
        updateFontComponents(for: tag, fontSize: fontSize)
    }

    func undo(tag: Int, alignment: NSTextAlignment) {
        updateAlignmentComponents(for: tag, state: alignment)
    }

    func undo(tag: Int, backgroundColor: UIColor) {
        updateBackgroundColorComponents(for: tag, backgroundColor: backgroundColor)
    }

    func undoChange(tag: Int, value: String) {
        guard let textOverlay = textOverlay(by: tag) else { return }
        textOverlay.text = value
        adjustTextOverlaySize(tag: tag)
    }
}
