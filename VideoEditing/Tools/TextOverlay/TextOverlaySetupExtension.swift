//
//  TextOverlaySetupExtension.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

extension TextOverlay {

    func setupTextOverlay() {
        let textOverlay = UITextView()

        addTextOverlay(textOverlay, isUndoAvailable: true)

        textOverlay.translatesAutoresizingMaskIntoConstraints = false
        textOverlay.font = UIFont.systemFont(ofSize: CGFloat(constants.textOverlay_standardFontSize), weight: .regular)

        guard let font = textOverlay.font else { return }
        let textSize = textOverlay.text.sizeOfString(usingFont: font)

        let textOverlayXAxisAnchor = textOverlay.centerXAnchor.constraint(equalTo: toolViewControllerDelegate.view.centerXAnchor)
        let textOverlayYAxisAnchor = textOverlay.centerYAnchor.constraint(equalTo: toolViewControllerDelegate.view.centerYAnchor)

        xAxisAnchors.append(textOverlayXAxisAnchor)
        yAxisAnchors.append(textOverlayYAxisAnchor)

        [xAxisAnchors.last,
         yAxisAnchors.last,
         textOverlay.widthAnchor.constraint(equalToConstant: constants.textOverlay_initialWidth),
         textOverlay.heightAnchor.constraint(equalToConstant: textSize.height)
            ].forEach { $0?.isActive = true }

        textOverlay.backgroundColor = .clear
        textOverlay.textColor = .white
        textOverlay.isScrollEnabled = false
        textOverlay.delegate = self
        textOverlay.tag = textOverlays.count
        textOverlay.textAlignment = .center
        textOverlay.autocorrectionType = .no
        textOverlay.roundCorners(.all, radius: constants.textOverlay_cornerRadius)
        textOverlaysPosition[textOverlay.tag] = textOverlay.center

        textOverlay.becomeFirstResponder()

        toolViewControllerDelegate.didAddTextOverlay = true

        textOverlays.append(textOverlay)
    }

    func setupManipulationView() {
        manipulationView = TextOverlayManipulationViewImp(parentView: toolViewControllerDelegate.view,
                                                          delegate: self,
                                                          shouldShowAlignment: true,
                                                          shouldShowBackgroundColor: true,
                                                          shouldShowFontStyle: true)
    }

    func updateComponents(for tag: Int) {
        slider?.tag = tag
        self.tag = tag

        guard let backgroundColor = textOverlay(by: tag)?.backgroundColor else { return }
        updateBackgroundColorComponents(for: tag, backgroundColor: backgroundColor)

        guard let alignmentState = textOverlay(by: tag)?.textAlignment else { return }
        updateAlignmentComponents(for: tag, state: alignmentState)

        guard let fontSize = textOverlay(by: tag)?.font?.pointSize else { return }
        updateFontComponents(for: tag, fontSize: fontSize)
    }

    func setupDarkBackgroundView() {
        if !toolViewControllerDelegate.view.subviews.filter({ $0.tag == constants.textOverlay_darkBackgroundTag }).isEmpty { return }
        darkBackground = UIView(frame: toolViewControllerDelegate.view.frame)

        guard let darkBackground = darkBackground else { return }

        darkBackground.backgroundColor = .black
        darkBackground.tag = constants.textOverlay_darkBackgroundTag
        darkBackground.alpha = 0
        toolViewControllerDelegate.view.addSubview(darkBackground)
    }

    func setupSlider() {

        if let slider = toolViewControllerDelegate.view.subviews.first(where: {$0 is TextOverlaySliderImp}) {
            self.toolViewControllerDelegate.view.bringSubviewToFront(slider)
            return
        }

        slider = TextOverlaySliderImp(parentView: toolViewControllerDelegate.view,
                                      delegate: self,
                                      minimumValue: constants.textOverlay_minFontSize,
                                      maximumValue: constants.textOverlay_maxFontSize,
                                      startValue: constants.textOverlay_standardFontSize)
    }

    private func standardTextFont() {
        textOverlayTextFont?.tag = textOverlays.count
    }

    func setupCenterXLine() {
        if centerXLine == nil {

            if toolViewControllerDelegate.view.subviews.filter({ $0.tag == constants.textOverlay_centerYLineTag }).first != nil {
                showCenterXLine()
                return
            }

            centerXLine = UIView()
            guard let centerXLine = centerXLine else { return }

            toolViewControllerDelegate.view.addSubview(centerXLine)

            centerXLine.translatesAutoresizingMaskIntoConstraints = false

            [centerXLine.widthAnchor.constraint(equalToConstant: constants.textOverlay_centerYLineWidth),
             centerXLine.heightAnchor.constraint(equalTo: toolViewControllerDelegate.view.heightAnchor),
             centerXLine.centerXAnchor.constraint(equalTo: toolViewControllerDelegate.view.centerXAnchor)
                ].forEach { $0.isActive = true }

            centerXLine.backgroundColor = .white
            centerXLine.tag = constants.textOverlay_centerYLineTag
            centerXLine.alpha = 0
            return
        }

        showCenterXLine()
    }

    private func showCenterXLine() {
        UIView.animate(withDuration: constants.textOverlay_basicAnimationDuration,
                       delay: constants.textOverlay_basicAnimationDelay,
                       options: .curveEaseIn,
                       animations: {
                        self.centerXLine?.alpha = 1
        }, completion: nil)
    }

    func hideCenterXLine() {
        UIView.animate(withDuration: constants.textOverlay_basicAnimationDuration,
                       delay: constants.textOverlay_basicAnimationDelay,
                       options: .curveEaseIn,
                       animations: {
                        self.centerXLine?.alpha = 0
        }, completion: nil)
    }

    func setupSnapping(for tag: Int) {
        if animator == nil { animator = UIDynamicAnimator(referenceView: toolViewControllerDelegate.view) }
        removeSnapping()

        guard let textOverlay = textOverlay(by: tag) else { return }
        snapping = UISnapBehavior(item: textOverlay, snapTo: CGPoint(x: toolViewControllerDelegate.view.center.x, y: textOverlay.center.y))

        if animator?.behaviors.isEmpty == true, let snapping = snapping {
            animator?.addBehavior(snapping)
        }
    }

    func removeSnapping() {
        animator?.removeAllBehaviors()
    }

    func shouldShowDarkBackground(_ shouldShow: Bool, for tag: Int) {
        UIView.animate(withDuration: constants.textOverlay_basicAnimationDuration,
                       delay: constants.textOverlay_basicAnimationDelay,
                       options: .curveEaseIn,
                       animations: {
                        self.darkBackground?.alpha = shouldShow ? constants.textOverlay_darkBackgroungAlpha : 0
                        shouldShow ? self.decreaseAlphaForUnselectedTextOverlays(selectedTextViewTag: tag) : self.setStandardAlphaForTextOverlays()
        }, completion: nil)
    }

    private func decreaseAlphaForUnselectedTextOverlays(selectedTextViewTag tag: Int) {
        toolViewControllerDelegate.view.subviews.filter { $0 is UITextView && $0.tag != tag }.forEach { $0.alpha = 0.5 }
    }

    private func setStandardAlphaForTextOverlays() {
        toolViewControllerDelegate.view.subviews.filter { $0 is UITextView }.forEach { $0.alpha = 1 }
    }

    func relocate(_ shouldRelocate: Bool, selectedTextViewTag tag: Int) {

        guard let xPosition = textOverlaysPosition[tag]?.x else { return }
        guard let yPosition = textOverlaysPosition[tag]?.y else { return }

        xAxisAnchors[tag].constant = shouldRelocate ? 0 : xPosition
        yAxisAnchors[tag].constant = shouldRelocate ? -200 : yPosition

        UIView.animate(withDuration: constants.textOverlay_basicAnimationDuration,
                       delay: constants.textOverlay_basicAnimationDelay,
                       options: .curveEaseIn,
                       animations: {
                        self.toolViewControllerDelegate.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension TextOverlay: ColorPaletteDelegate {
    var selectedColor: UIColor {
        guard let tag = tag,
            let textColor = textOverlay(by: tag)?.textColor else { return .white}
        return textColor
    }

    func didSet(color: UIColor) {
        if let tag = tag {
            changeTextColor(for: tag, selectedColor: color)
        }
    }
}

extension TextOverlay {

    func setupTapGestureIfNeeded(for textOverlay: UITextView) {
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))

        if textOverlay.gestureRecognizers?.filter({ $0 == tapGesture }).first == nil {
            tapGesture?.delegate = self
            if let tap = tapGesture { textOverlay.addGestureRecognizer(tap) }
        }
    }

    func setupPanGestureIfNeeded(for textOverlay: UITextView) {
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))

        if textOverlay.gestureRecognizers?.filter({ $0 == panGesture }).first == nil {
            panGesture?.delegate = self
            if let pan = panGesture { textOverlay.addGestureRecognizer(pan) }
        }
    }
}
