//
//  Line.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

struct Line {
    let strokeWidth: Float
    let color: UIColor
    let blendMode: CGBlendMode
    let hasNeon: Bool
    var points: [CGPoint]
}
