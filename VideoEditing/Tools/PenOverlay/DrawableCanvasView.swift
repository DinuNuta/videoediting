//
//  DrawableCanvasView.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

protocol DrawableCanvasView: AnyObject {
    func undo()
    func clear()
    func setStrokeWidth(width: Float)
    func setStrokeColor(color: UIColor)
}

enum PenType {
    case samplePen
    case neonPen
    case marker
    case eraser
}

extension DrawableCanvasViewImp: CustomView {}

class DrawableCanvasViewImp: UIView {

    var view: UIView!

    @IBOutlet weak var toolView: UIView!
    @IBOutlet weak var topToolConstraint: NSLayoutConstraint!
    @IBOutlet var selctableToolItems: [UIButton]!

    weak var toolViewControllerDelegate: (ToolViewDelegate & UIViewController)?
    var undoManipulationManager: UndoManipulationManager?
    fileprivate let strokeWidthMultiplier: Float = 10
    lazy var slider: TextOverlaySliderImp? = {
        return  TextOverlaySliderImp(parentView: self.view, //self.toolViewControllerDelegate?.view ?? self.view,
            delegate: self,
            minimumValue: constants.drawLine_minStrokeWidth,
            maximumValue: constants.drawLine_maxStrokeWidth,
            startValue: self.strokeWidth*strokeWidthMultiplier)
    }()

    fileprivate weak var penMarker: UIView?

    fileprivate var lines = [Line]()
    fileprivate var strokeColor = UIColor.gray
    fileprivate var strokeWidth: Float = constants.drawLine_strokeWidth
    fileprivate var strokeAlpha: Double {return self.currentPenType == .marker ? 0.7 : 1.0 }

    lazy var currentPenType: PenType = .samplePen
    var isErasing: Bool {return self.currentPenType == .eraser}
    var isNeon: Bool {return self.currentPenType == .neonPen}

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetup()
        configView()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        configView()
    }

    init() {
        super.init(frame: CGRect.zero)

    }

    // MARK: Draw
    override func draw(_ rect: CGRect) {
        super.draw(rect)

        guard let context = UIGraphicsGetCurrentContext() else { return }
        DispatchQueue.global(qos: .userInteractive).sync {
            self.lines.forEach { (line) in
                //for neon line
                if line.hasNeon {
                    context.setStrokeColor(line.color.withAlphaComponent(0.7).cgColor)
                    context.setLineWidth(CGFloat(line.strokeWidth))
                    context.setLineCap(.round)
                    context.setLineJoin(.round)
                    context.setBlendMode(.normal)
                    context.setShadow(offset: .zero, blur: 18, color: line.color.cgColor)

                    for (i, p) in line.points.enumerated() {
                        if i == 0 {
                            context.move(to: p)
                        } else {
                            context.addLine(to: p)
                        }
                    }
                    context.strokePath()
                }
                //for sample line
                let strokeWidth = line.hasNeon ? max(line.strokeWidth - 2, 1) : line.strokeWidth
                context.setShadow(offset: .zero, blur: 0, color: nil)
                context.setStrokeColor(line.hasNeon ? UIColor.white.cgColor : line.color.cgColor)
                context.setLineWidth(CGFloat(strokeWidth))
                context.setLineCap(.round)
                context.setLineJoin(.round)
                context.setBlendMode(line.blendMode)
                for (i, p) in line.points.enumerated() {
                    if i == 0 {
                        context.move(to: p)
                    } else {
                        context.addLine(to: p)
                    }
                }
                context.strokePath()
            }
        }
    }

    // MARK: touches
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.animateToolView(shouldShow: false)
        let newLine = Line.init(strokeWidth: strokeWidth, color: strokeColor,
                                blendMode: isErasing ? .clear : .normal,
                                hasNeon: isNeon,
                                points: [])
        lines.append(newLine)
        if let undoManager = self.undoManipulationManager { undoManager.append(manipulation: .added(component: .draw)) }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: nil) else { return }
        guard var lastLine = lines.popLast() else { return }

        let lastTouchPoint: CGPoint = lastLine.points.last ?? .zero
        lastLine.points.append(point)
        lines.append(lastLine)
        let rect = calculateRectBetween(lastPoint: lastTouchPoint, newPoint: point)
        setNeedsDisplay(rect)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.animateToolView(shouldShow: true)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.animateToolView(shouldShow: true)
    }

    func calculateRectBetween(lastPoint: CGPoint, newPoint: CGPoint) -> CGRect {
        let lineWidth = CGFloat(strokeWidth + (self.isNeon ? 22 : 1))
        let originX = min(lastPoint.x, newPoint.x) - (lineWidth / 2)
        let originY = min(lastPoint.y, newPoint.y) - (lineWidth / 2)

        let maxX = max(lastPoint.x, newPoint.x) + (lineWidth / 2)
        let maxY = max(lastPoint.y, newPoint.y) + (lineWidth / 2)

        let width = maxX - originX
        let height = maxY - originY

        return CGRect(x: originX, y: originY, width: width, height: height)
    }

    // MARK: setup
    func configView() {
        self.backgroundColor = .clear
        self.slider?.tintColor = strokeColor
    }

    func setup(toolViewControllerDelegate: ToolViewDelegate & UIViewController,
               undoManipulationManager: UndoManipulationManager) {
        self.toolViewControllerDelegate = toolViewControllerDelegate
        self.undoManipulationManager = undoManipulationManager
    }

    // MARK: actions tool
    @IBAction func undoAction(_ sender: UIButton) {
        //        self.undo()
        self.undoManipulationManager?.removeLast()
    }

    @IBAction func samplePen(_ sender: UIButton) {
        self.currentPenType = .samplePen
        updateConfig()
        sender.isSelected = true
        slider?.maximumValue = constants.drawLine_maxStrokeWidth
    }

    @IBAction func markerPen(_ sender: UIButton) {
        self.currentPenType = .marker
        updateConfig()
        sender.isSelected = true
        slider?.maximumValue = constants.drawLine_maxEraserWidth
    }

    @IBAction func neonPen(_ sender: UIButton) {
        self.currentPenType = .neonPen
        updateConfig()
        sender.isSelected = true
        slider?.maximumValue = constants.drawLine_maxStrokeWidth
    }

    @IBAction func eraseAction(_ sender: UIButton) {
        self.currentPenType = .eraser
        updateConfig()
        sender.isSelected = true
        slider?.maximumValue = constants.drawLine_maxEraserWidth
    }

    private func updateConfig() {
        selctableToolItems.forEach({$0.isSelected = false})
        setStrokeColor(color: strokeColor)
    }

    @IBAction func doneAction(_ sender: UIButton) {
        toolViewControllerDelegate?.triggerToolIndicator(tool: .draw, delegate: self)
    }

    fileprivate func animateToolView(shouldShow show: Bool) {
        toolViewControllerDelegate?.colorPaletteShould(Show: show)
        DispatchQueue.main.async {
            let adjustment: CGFloat = show ? 0 : -constants.toolbarHeight_adjuststment
            self.topToolConstraint.constant = adjustment
            let adjustmentSlider: CGFloat = show ? constants.textOverlay_adjustment : -constants.textOverlay_adjustment
            self.slider?.moveHorizontally(by: adjustmentSlider)
            UIView.animate(withDuration: constants.textOverlay_basicAnimationDuration,
                           delay: constants.textOverlay_basicAnimationDelay,
                           options: .curveEaseIn,
                           animations: {
                            self.toolView.alpha = show ? 1 : 0.001
                            self.slider?.alpha = show ? 1 : 0.001
                            self.layoutIfNeeded()
            }, completion: nil)
        }
    }

}

extension DrawableCanvasViewImp {
    // MARK: Dot Marker
    fileprivate func getDotMarker(strokeWidth: Float, backColor: UIColor) -> UIView {
        let width = CGFloat(strokeWidth + 16)
        let frame = CGRect.init(x: 0, y: 0, width: width, height: width)
        let view = UIView.init(frame: frame)
        view.center = self.center
        view.borderColor = .white
        view.borderWidth = 4.0
        view.roundCorner = true
        view.backgroundColor = backColor
        self.addSubview(view)
        return view
    }

    func showPenMarker() {
        self.penMarker = getDotMarker(strokeWidth: self.strokeWidth, backColor: isErasing ? .white : self.strokeColor)
    }

    func removePenMarker() {
        self.penMarker?.removeFromSuperview()
    }

    func updatePenMarker(by strokeWidth: Float) {
        let width = CGFloat(strokeWidth + 16)
        let size = CGSize(width: width, height: width)
        self.penMarker?.frame.size = size
        self.penMarker?.roundCorner = true
        self.penMarker?.center = self.center
    }
}

extension DrawableCanvasViewImp: DrawableCanvasView {
    func setStrokeWidth(width: Float) {
        self.strokeWidth = width
        updatePenMarker(by: self.strokeWidth)
    }

    func setStrokeColor(color: UIColor) {
        self.strokeColor = color.withAlphaComponent(CGFloat(strokeAlpha))
        slider?.tintColor = color
    }

    func undo() {
        _ = lines.popLast()
        setNeedsDisplay()
    }

    func clear() {
        lines.removeAll()
        setNeedsDisplay()
    }
}

// MARK: ToolTriggerDelegate
extension DrawableCanvasViewImp: ToolTriggerDelegate {
    func isActive() -> Bool {
        return self.isUserInteractionEnabled
    }

    func activate(_ shouldActivate: Bool) {
        self.toolViewControllerDelegate?.subscribeToColorPalete(item: shouldActivate ? self : nil)
        DispatchQueue.main.async {
            self.isUserInteractionEnabled = shouldActivate
        }
        self.toolViewControllerDelegate?.shouldHidePreviewTools(shouldActivate)
        self.animateToolView(shouldShow: shouldActivate)
    }
}

// MARK: ColorPaletteDelegate
extension DrawableCanvasViewImp: ColorPaletteDelegate {
    var selectedColor: UIColor {
        return self.strokeColor
    }

    func didSet(color: UIColor) {
        setStrokeColor(color: color)
    }
}

// MARK: TextOverlaySliderDelegate
extension DrawableCanvasViewImp: TextOverlaySliderDelegate {
    func didChangeSliderValue(_ sender: UISlider, event: UIEvent) {
        setStrokeWidth(width: sender.value/strokeWidthMultiplier)
    }

    func didTouchDownSlider(_ sender: UISlider) {
        //add a circle with selected color marked width of line
        showPenMarker()
    }

    func didTouchUPSlider(_ sender: UISlider) {
        //hide circle marked width of line
        removePenMarker()
    }
}
