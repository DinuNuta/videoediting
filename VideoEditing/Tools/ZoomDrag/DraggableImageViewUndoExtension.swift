//
//  DraggableImageViewUndoExtension.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

extension DraggableImageView {

    func undoChangePosition(center: CGPoint) {
        self.center = center
    }

    func undoChangeTransform(transform: CGAffineTransform) {
        self.transform = transform
    }
}
