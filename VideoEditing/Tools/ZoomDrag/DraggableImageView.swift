//
//  DraggableImageView.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit
//import Toucan

class DraggableImageView: UIImageView, UIGestureRecognizerDelegate {

    var undoManipulationManager: UndoManipulationManager
    var previousLocation = CGPoint.zero
    var scissorsMoveRecognizer: UIPanGestureRecognizer?
    var pinchRecognizer: UIPinchGestureRecognizer?
    var panRecognizer: UIPanGestureRecognizer?
    var rotationRecognizer: UIRotationGestureRecognizer?
    var scissorsPath: UIBezierPath?

    private lazy var lineShape: CAShapeLayer = {
        let lineShape = CAShapeLayer()
        lineShape.strokeColor = UIColor.white.withAlphaComponent(0.3).cgColor
        lineShape.lineWidth = 30.0
        lineShape.fillColor = UIColor.clear.cgColor

        return lineShape
    }()

    init(baseView: UIView,
         image: UIImage,
         undoManipulationManager: UndoManipulationManager) {

        self.undoManipulationManager = undoManipulationManager
        super.init(image: image)
        initialSetup(baseView: baseView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initialSetup(baseView: UIView) {
        let scale = max(baseView.frame.width / self.frame.width, baseView.frame.height / self.frame.height)
        transform = transform.scaledBy(x: scale, y: scale)

        isUserInteractionEnabled = true
        center = baseView.center
        previousLocation = self.center
        contentMode = .center

        rotationRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(handleRotation(_:)))
        rotationRecognizer?.delegate = self
        if let reco = rotationRecognizer {
            baseView.addGestureRecognizer(reco)
        }

        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        panRecognizer?.delegate = self
        panRecognizer?.minimumNumberOfTouches = 2
        if let reco = panRecognizer {
            baseView.addGestureRecognizer(reco)
        }

        scissorsMoveRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleScissorsPan(_:)))
        scissorsMoveRecognizer?.delegate = self
        scissorsMoveRecognizer?.isEnabled = false
        if let reco = scissorsMoveRecognizer {
            baseView.addGestureRecognizer(reco)
        }

        pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch(_:)))
        pinchRecognizer?.delegate = self
        if let reco = pinchRecognizer {
            baseView.addGestureRecognizer(reco)
        }
    }

    func setupConstraints(backgroundView: UIView) {

        backgroundView.addSubview(self)

        translatesAutoresizingMaskIntoConstraints = false

        [topAnchor.constraint(equalTo: backgroundView.topAnchor),
         leftAnchor.constraint(equalTo: backgroundView.leftAnchor),
         rightAnchor.constraint(equalTo: backgroundView.rightAnchor),
         bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor)
            ].forEach { $0.isActive = true }
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

//Crop tool setup
extension DraggableImageView {

    @objc func handlePan(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: superview)
        center = CGPoint(x: center.x + translation.x, y: center.y + translation.y)
        sender.setTranslation(.zero, in: superview)

        if sender.state == .began {
            undoManipulationManager.append(manipulation: .changedPosition(component: .image, center: center))
        }
    }

    @objc func handlePinch(_ sender: UIPinchGestureRecognizer) {
        transform = transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1

        if sender.state == .began {
            undoManipulationManager.append(manipulation: .changedTransform(component: .image, transform: transform))
        }
    }

    @objc func handleRotation(_ sender: UIRotationGestureRecognizer) {
        transform = transform.rotated(by: sender.rotation)
        sender.rotation = 0

        if sender.state == .began {
            undoManipulationManager.append(manipulation: .changedTransform(component: .image, transform: transform))
        }
    }

    public func crop(with rect: CGRect) -> UIImage {
        if let fixedImage = self.image?.cgImageWithFixedOrientation() {
            let imageRef = fixedImage.transformedImage(transform,
                                                       zoomScaleHeight: self.transform.yScale,
                                                       zoomScaleWidth: self.transform.xScale,
                                                       sourceSize: self.image!.size,
                                                       cropRect: rect,
                                                       imageView: self)

            return UIImage(cgImage: imageRef)
        }
        return UIImage()
    }
}

extension UIImage {
    func crop( rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale

        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
}

//Scissors tool setup
extension DraggableImageView {

    @objc func handleScissorsPan(_ gesture: UIPanGestureRecognizer) {
        //    let currentPanPoint = gesture.location(in: self.superview!)
        //
        //    switch (gesture.state) {
        //      case .began:
        //        lineShape.path = nil
        //        scissorsPath = UIBezierPath()
        //        lineShape.removeFromSuperlayer()
        //        self.superview!.layer.addSublayer(lineShape)
        //      case .changed:
        //        if scissorsPath?.bounds.width == 0 && scissorsPath?.bounds.height == 0 {
        //          scissorsPath?.move(to: currentPanPoint)
        //        }
        //        scissorsPath?.addLine(to: currentPanPoint)
        //        lineShape.path = scissorsPath?.cgPath
        //        previousLocation = currentPanPoint
        //      case .ended:
        //        scissorsPath?.close()
        //      default:
        //        break
        //    }
    }

    public func getScissorsCut() -> UIImage {
        return self.image?.imageByApplyingClippingBezierPath(scissorsPath ?? UIBezierPath(), self) ?? UIImage()
    }
}

//Cropping + scissors methods
private extension UIImage {

    func imageByApplyingClippingBezierPath(_ path: UIBezierPath, _ imageView: UIImageView) -> UIImage {
        // Mask image using path
        let maskedImage = imageByApplyingMaskingBezierPath(path, imageView)

        // Crop image to frame of path
        let cgImage = maskedImage.cgImage!.cropping(to: path.bounds)
        if cgImage == nil {
            return UIImage()
        }
        return UIImage(cgImage: cgImage!)
    }

    private func imageByApplyingMaskingBezierPath(_ path: UIBezierPath, _ imageView: UIImageView) -> UIImage {
        // Define graphic context (canvas) to paint on
        let scale = max(self.size.width / imageView.frame.width,
                        self.size.height / imageView.frame.height)
        let scaledSize = CGSize(width: size.width / scale, height: size.height / scale)
        UIGraphicsBeginImageContext(scaledSize)
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()

        // Set the clipping mask
        path.addClip()
        draw(in: CGRect(x: path.bounds.origin.x / scale,
                        y: path.bounds.origin.y / scale,
                        width: self.size.width / scale,
                        height: self.size.height / scale))

        let maskedImage = UIGraphicsGetImageFromCurrentImageContext()!

        // Restore previous drawing context
        context.restoreGState()
        UIGraphicsEndImageContext()

        return maskedImage
    }

    func cgImageWithFixedOrientation() -> CGImage? {
        guard let cgImage = self.cgImage, let colorSpace = cgImage.colorSpace else {
            return nil
        }

        if self.imageOrientation == UIImage.Orientation.up {
            return self.cgImage
        }

        let width  = self.size.width
        let height = self.size.height

        var transform = CGAffineTransform.identity

        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: width, y: height)
            transform = transform.rotated(by: CGFloat.pi)

        case .left, .leftMirrored:
            transform = transform.translatedBy(x: width, y: 0)
            transform = transform.rotated(by: 0.5 * CGFloat.pi)

        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: height)
            transform = transform.rotated(by: -0.5 * CGFloat.pi)

        case .up, .upMirrored:
            break
        }

        switch self.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)

        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)

        default:
            break
        }

        guard let context = CGContext(
            data: nil,
            width: Int(width),
            height: Int(height),
            bitsPerComponent: cgImage.bitsPerComponent,
            bytesPerRow: 0,
            space: colorSpace,
            bitmapInfo: UInt32(cgImage.bitmapInfo.rawValue)
            ) else {
                return nil
        }

        context.concatenate(transform)

        switch self.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            context.draw(cgImage, in: CGRect(x: 0, y: 0, width: height, height: width))

        default:
            context.draw(cgImage, in: CGRect(x: 0, y: 0, width: width, height: height))
        }

        // And now we just create a new UIImage from the drawing context
        guard let newCGImg = context.makeImage() else {
            return nil
        }

        return newCGImg
    }
}

private extension CGImage {

    func transformedImage(_ transform: CGAffineTransform,
                          zoomScaleHeight: CGFloat,
                          zoomScaleWidth: CGFloat,
                          sourceSize: CGSize,
                          cropRect: CGRect,
                          imageView: UIImageView) -> CGImage {
        let imageViewSize = imageView.bounds.size
        let expectedWidth = floor(sourceSize.width / imageViewSize.width * cropRect.width) / zoomScaleWidth
        let expectedHeight = floor(sourceSize.height / imageViewSize.height * cropRect.height) / zoomScaleHeight
        let outputSize = CGSize(width: expectedWidth, height: expectedHeight)
        let bitmapBytesPerRow = 0

        let context = CGContext(data: nil,
                                width: Int(outputSize.width),
                                height: Int(outputSize.height),
                                bitsPerComponent: self.bitsPerComponent,
                                bytesPerRow: bitmapBytesPerRow,
                                space: self.colorSpace!,
                                bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)
        context?.setFillColor(UIColor.black.cgColor)
        context?.fill(CGRect(x: CGFloat.zero,
                             y: CGFloat.zero,
                             width: outputSize.width,
                             height: outputSize.height))

        var uiCoords = CGAffineTransform(scaleX: outputSize.width / cropRect.width,
                                         y: outputSize.height / cropRect.height)
        uiCoords = uiCoords.translatedBy(x: cropRect.width.half,
                                         y: cropRect.height.half)
        uiCoords = uiCoords.scaledBy(x: 1.0, y: -1.0)

        context?.concatenate(uiCoords)
        context?.concatenate(transform)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.draw(self, in: CGRect(x: -(imageViewSize.width.half),
                                       y: -(imageViewSize.height.half),
                                       width: imageViewSize.width,
                                       height: imageViewSize.height))
        let result = context!.makeImage()!

        return result
    }
}

private extension CGAffineTransform {
    var xScale: CGFloat { return sqrt(self.a * self.a + self.c * self.c) }
    var yScale: CGFloat { return sqrt(self.b * self.b + self.d * self.d) }
    var rotation: CGFloat { return CGFloat(atan2(Double(self.b), Double(self.a))) }
    // .tx and .ty are already available in the transform
}

extension DraggableImageView: ToolTriggerDelegate {

    func isActive() -> Bool {
        guard let scissorsMoveRecognizer = scissorsMoveRecognizer else { return false }
        return scissorsMoveRecognizer.isEnabled
    }

    func activate(_ shouldActivate: Bool) {
        lineShape.path = nil
        scissorsPath = UIBezierPath()
        lineShape.removeFromSuperlayer()
        scissorsMoveRecognizer?.isEnabled = shouldActivate
        rotationRecognizer?.isEnabled = shouldActivate
        panRecognizer?.isEnabled = shouldActivate
        pinchRecognizer?.isEnabled = shouldActivate
    }
}
