//
//  Filters.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

class Filters {

    var delegateView: UIView?
    var primeImageView: UIImageView?
    var filterTitleLabel: UILabel?
    var primeImage: UIImage?

    var modifiedImage: UIImage?
    var filterIndex = 0

    var swipeLeftGesture: UISwipeGestureRecognizer?
    var swipeRightGesture: UISwipeGestureRecognizer?

    var selectedFilter: AvailableFilter = .original
    var selectedEffect: NativeEffect = .original

    init(delegateView: UIView?, primeImageView: UIImageView?, filterTitleLabel: UILabel?) {
        self.delegateView = delegateView
        self.primeImageView = primeImageView
        self.filterTitleLabel = filterTitleLabel
        self.primeImage = primeImageView?.image

        addSwipeGestures()
    }

    func addSwipeGestures() {
        swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        if let swipeLeftGesture = swipeLeftGesture {
            swipeLeftGesture.direction = .left
            delegateView?.addGestureRecognizer(swipeLeftGesture)
        }

        swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        if let swipeRightGesture = swipeRightGesture {
            swipeRightGesture.direction = .right
            delegateView?.addGestureRecognizer(swipeRightGesture)
        }
    }

    @objc private func handleSwipe(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .left: incrementFilterIndex()
        case .right: reduceFilterIndex()
        default: break
        }

        // Removing all previous filters applied
        primeImageView?.image = primeImage

        selectFilter()
        selectEffect()
        applyFilter()

        setModifiedImageWithTitle()
    }

    func setModifiedImageWithTitle() {
        primeImageView?.image = modifiedImage

        filterTitleLabel?.text = selectedFilter.rawValue
        filterTitleLabel?.isHidden = false
        filterTitleLabel?.alpha = 1

        UIView.animate(withDuration: 1) {
            self.filterTitleLabel?.alpha = 0
        }
    }

    private func incrementFilterIndex() {
        let filtersCount = AvailableFilter.allValues.count - 1
        filterIndex = filterIndex >= filtersCount ? filtersCount : filterIndex + 1
    }

    private func reduceFilterIndex() {
        filterIndex = filterIndex <= 0 ? 0 : filterIndex - 1
    }

    private func selectFilter() {
        guard let selectedFilter = AvailableFilter.allValues.enumerated().filter({ (index, _) -> Bool in
            index == filterIndex && filterIndex <= AvailableFilter.allValues.count }).first?.element else {
                self.selectedFilter = .original
                return
        }
        self.selectedFilter = selectedFilter
    }

    private func selectEffect() {
        switch selectedFilter {
        case .original: selectedEffect = .original
        case .mono: selectedEffect = .mono
        case .noir: selectedEffect = .noir
        case .silvertone: selectedEffect = .silvertone
        case .dramatic,
             .dramaticCool,
             .dramaticWarm,
             .vivid,
             .vividCool,
             .vividWarm: selectedEffect = .custom
        }
    }

    private func applyFilter() {
        if selectedEffect == .original {
            modifiedImage = primeImage
            return
        }

        guard let ciFilter = CIFilter(name: selectedEffect.rawValue) else { return }

        selectedEffect != .custom
            ? setImageWithFilter(ciFilter: ciFilter)
            : configureCustomEffect()
    }

    private func setImageWithFilter(ciFilter: CIFilter) {
        guard let primeImage = primeImageView?.image else { return }

        guard let cgImage = primeImage.cgImage,
            let openGLContext = EAGLContext(api: .openGLES3) else { return }

        let context = CIContext(eaglContext: openGLContext)
        let ciImage = CIImage(cgImage: cgImage)

        ciFilter.setValue(ciImage, forKey: kCIInputImageKey)

        guard let output = ciFilter.value(forKey: kCIOutputImageKey) as? CIImage,
            let cgiImageResult = context.createCGImage(output, from: output.extent) else { return }

        let filteredImage = UIImage(cgImage: cgiImageResult, scale: primeImage.scale, orientation: primeImage.imageOrientation)

        modifiedImage = filteredImage
    }

    private func configureCustomEffect() {
        // Brightness from -1 to 1
        // Contrast from 0 to 4
        // Saturation from 0 to 2

        switch selectedFilter {
        case .vivid: applyCustomFilter(parameters: [kCIInputBrightnessKey: 0.06, kCIInputContrastKey: 1.15, kCIInputSaturationKey: 1.2])
        case .vividWarm: applyCustomFilter(parameters: [kCIInputBrightnessKey: 0.06, kCIInputContrastKey: 1.15, kCIInputSaturationKey: 1.4])
        case .vividCool: applyCustomFilter(parameters: [kCIInputBrightnessKey: 0.06, kCIInputContrastKey: 1.15, kCIInputSaturationKey: 0.6])
        case .dramatic: applyCustomFilter(parameters: [kCIInputBrightnessKey: -0.02, kCIInputContrastKey: 1.05, kCIInputSaturationKey: 1.2])
        case .dramaticWarm: applyCustomFilter(parameters: [kCIInputBrightnessKey: -0.02, kCIInputContrastKey: 1.05, kCIInputSaturationKey: 1.4])
        case .dramaticCool: applyCustomFilter(parameters: [kCIInputBrightnessKey: -0.02, kCIInputContrastKey: 1.05, kCIInputSaturationKey: 0.6])
        default: break
        }
    }

    private func applyCustomFilter(parameters: [String: Double]? = nil) {
        guard let ciFilter = CIFilter(name: "CIColorControls", parameters: parameters) else { return }
        setImageWithFilter(ciFilter: ciFilter)
    }
}

extension Filters {

    enum AvailableFilter: String {
        case original = "ORIGINAL"
        case vivid = "VIVID"
        case vividWarm = "VIVID WARM"
        case vividCool = "VIVID COOL"
        case dramatic = "DRAMATIC"
        case dramaticWarm = "DRAMATIC WARM"
        case dramaticCool = "DRAMATIC COOL"
        case mono = "MONO"
        case silvertone = "SILVERTONE"
        case noir = "NOIR"

        static let allValues: [AvailableFilter] =
            [.original,
             .vivid,
             .vividWarm,
             .vividCool,
             .dramatic,
             .dramaticWarm,
             .dramaticCool,
             .mono,
             .silvertone,
             .noir ]
    }

    enum NativeEffect: String {
        case original = "Original"
        case mono = "CIPhotoEffectMono"
        case noir = "CIPhotoEffectNoir"
        case silvertone = "CIPhotoEffectTonal"
        case custom = "CIColorControls"

        static let allValues: [NativeEffect] =
            [.original,
             .mono,
             .noir,
             .silvertone,
             .custom ]
    }
}

extension Filters: ToolTriggerDelegate {

    func isActive() -> Bool {
        guard let swipeLeftGesture = swipeLeftGesture else { return false }
        guard let swipeRightGesture = swipeRightGesture else { return false }
        return swipeLeftGesture.isEnabled && swipeRightGesture.isEnabled
    }

    func activate(_ shouldActivate: Bool) {
        swipeLeftGesture?.isEnabled = shouldActivate
        swipeRightGesture?.isEnabled = shouldActivate
    }
}
