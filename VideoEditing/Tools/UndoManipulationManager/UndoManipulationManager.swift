//
//  UndoManipulationManager.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

protocol UndoManipulationManager {
    func append(manipulation: UndoManipulationManagerImp.Manipulation)
    func removeLast()
    func removeManipulationFrom(undoComponent: UndoComponent)

    var didManipulate: Bool { get }
}

class UndoManipulationManagerImp: UndoManipulationManager {

    weak var delegate: ManipulationComponent?
    private var manipulations: [Manipulation] = []
    private var lastRemovedManipulation: Manipulation?

    init(delegate: ManipulationComponent) {
        self.delegate = delegate
    }

    func append(manipulation: Manipulation) {
        manipulations.append(manipulation)
        delegate?.shouldShowUndoButton(true)
    }

    var didManipulate: Bool {
        return !manipulations.isEmpty
    }

    func removeLast() {
        if let manipulation = manipulations.last {
            delegate?.remove(manipulation: manipulation)
            lastRemovedManipulation = manipulation

            if didManipulate { manipulations.removeLast() }

            showUndoButtonIfNeeded()
        } else {
            guard let lastRemovedManipulation = lastRemovedManipulation else { return }
            delegate?.restore(manipulation: lastRemovedManipulation)
        }
    }

    func removeManipulationFrom(undoComponent: UndoComponent) {

        var modifiedManipulations = manipulations.filter {
            switch $0 {
            case .added(component: let component): if undoComponent == component { return true }
            case .changedAlignment(component: let component, alignment: _ ): if undoComponent == component { return true }
            case .changedFontSize(component: let component, fontSize: _ ): if undoComponent == component { return true }
            case .changedPosition(component: let component, center: _ ): if undoComponent == component { return true }
            case .changedTextOverlayBackgroundColor(component: let component, color: _ ): if undoComponent == component { return true }
            case .changedTransform(component: let component, transform: _ ): if undoComponent == component { return true }
            case .changedValue(component: let component, value: _ ): if undoComponent == component { return true }
            case .removed(component: let component): if undoComponent == component { return true }
            }
            return false
        }

        guard let lastRemovedManipulation = modifiedManipulations.last else { return }
        self.lastRemovedManipulation = lastRemovedManipulation
        modifiedManipulations.removeLast()
        manipulations = modifiedManipulations

        showUndoButtonIfNeeded()
    }

    private func showUndoButtonIfNeeded() {
        if !didManipulate { delegate?.shouldShowUndoButton(false) }
    }

    // Component with previous value
    enum Manipulation {
        case added(component: UndoComponent)
        case changedAlignment(component: UndoComponent, alignment: NSTextAlignment)
        case changedFontSize(component: UndoComponent, fontSize: CGFloat)
        case changedPosition(component: UndoComponent, center: CGPoint)
        case changedTextOverlayBackgroundColor(component: UndoComponent, color: UIColor)
        case changedTransform(component: UndoComponent, transform: CGAffineTransform)
        case changedValue(component: UndoComponent, value: String)
        case removed(component: UndoComponent)
    }
}

enum UndoComponent: Equatable {
    case image
    case gif(id: String)
    case textOverlay(tag: Int)
    case draw
}

protocol ManipulationComponent: AnyObject {
    func remove(manipulation: UndoManipulationManagerImp.Manipulation)
    func restore(manipulation: UndoManipulationManagerImp.Manipulation)
    func shouldShowUndoButton(_ shouldShow: Bool)
}
