//
//  PreviewPresenter.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import AVFoundation
import CoreLocation
import Photos
import PKHUD
import UIKit


//TODO: should refactor
var currentUploadingIndex = 0
var progressCellsToShow = 0
var progressCellsIsUploadingStatus: [Int: Bool] = [:]

protocol PreviewPresenter {

    var backgroundImage: UIImage? { get set }

    func getToolsGradientLayer(for rect: CGRect) -> CAGradientLayer
    func getPlayerLayer(for frame: CGRect) -> AVPlayerLayer?

    func isAudioVideoAvailable() -> Bool
    func changePlayerState()
    func initiate(action backgroundImage: UIImage?, draw: UIView?, textOverlay: CALayer?)
}

class PreviewPresenterImp: NSObject, PreviewPresenter {

    weak var view: PreviewView!
    var router: PreviewRouter!
    var audioVideoUrl: URL?
    var image: UIImage?
    var playerLayer: AVPlayerLayer?

    var videoComposer: VideoComposerImp!
    

    init(view: PreviewView, router: PreviewRouter, audioVideoUrl: URL?, image: UIImage?, videoComposer: VideoComposerImp) {
        self.view = view
        self.router = router
        self.audioVideoUrl = audioVideoUrl
        self.image = image
        self.videoComposer = videoComposer
        HUD.allowsInteraction = false
    }

    var backgroundImage: UIImage? {
        get { return image }
        set { image = newValue }
    }

    func getToolsGradientLayer(for rect: CGRect) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()

        gradient.colors = [UIColor.clear.cgColor,
                           UIColor.black.withAlphaComponent(0.7).cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0,
                                y: 0.0,
                                width: rect.width,
                                height: rect.height)

        return gradient
    }


    func getPlayerLayer(for frame: CGRect) -> AVPlayerLayer? {
        let url = audioVideoUrl ?? URL(fileURLWithPath: "")
        let player = AVPlayer(url: url)
        player.addObserver(view as! NSObject,
                           forKeyPath: "rate",
                           options: NSKeyValueObservingOptions(rawValue: 0),
                           context: nil)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.addObserver(view as! NSObject,
                                 forKeyPath: "videoRect",
                                 options: NSKeyValueObservingOptions(rawValue: 0),
                                 context: nil)
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspect
        playerLayer?.frame = CGRect(x: 0, y: 0,
                                    width: frame.width,
                                    height: frame.height)
        return playerLayer
    }

    func changePlayerState() {
        if playerLayer?.player?.rate != 0.0 {
            playerLayer?.player?.seek(to: CMTime.zero)
            playerLayer?.player?.pause()
        } else {
            playerLayer?.player?.play()
        }
    }

    func isAudioVideoAvailable() -> Bool {
        return audioVideoUrl != nil
    }


    @objc func videoSaved(videoPath: String, didFinishSavingWithError error: NSError?, contextInfo info: UnsafeMutableRawPointer) {

        if let error = error {
            DispatchQueue.main.async { HUD.hide() }
            self.view.displayAlert(error.localizedDescription)
        } else {
            DispatchQueue.main.async {
                HUD.hide()
                HUD.flash(.labeledSuccess(title: nil, subtitle: "Video saved successfully"), delay: 1.0)
            }
        }
    }
}

protocol ProgressDelegate {
    func update(progress: Float)
}

extension PreviewPresenterImp: ProgressDelegate {

    func update(progress: Float) {

        if progress < 1 {
            progressCellsIsUploadingStatus[currentUploadingIndex] = true
        }

        if progress == 1 {
            progressCellsIsUploadingStatus[currentUploadingIndex] = false
        }
    }
}

extension PreviewPresenterImp { // Content manipulation

    func initiate(action backgroundImage: UIImage?, draw: UIView?, textOverlay: CALayer?) {
        DispatchQueue.main.async {
            HUD.show(.progress)
        }
        let completion: (URL) -> Void = {[weak self] (url) in
            guard let self = self else {return}
            UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, #selector(PreviewPresenterImp.videoSaved(videoPath:didFinishSavingWithError:contextInfo:)), nil)
        }
        
        let _layers = [textOverlay].compactMap{$0}
        
        
        composeContent(audioVideoUrl: self.audioVideoUrl, draw: draw?.layer, aditionalLayers: _layers,
                       backgroundImage: backgroundImage, completion: completion)
        
//        if let _draw = draw {
//            textOverlay.flatMap({_draw.addSubview($0)})
//            composeContent(audioVideoUrl: self.audioVideoUrl, draw: _draw, backgroundImage: backgroundImage, completion: completion)
//        }else{
//            composeContent(audioVideoUrl: self.audioVideoUrl, draw: draw, backgroundImage: backgroundImage, completion: completion)
//        }
    }

    private func startWaitingQueue(_ completion: @escaping (Bool) -> Void) {
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in

            if progressCellsIsUploadingStatus.filter({ _, value in
                value == true
            }).isEmpty {
                timer.invalidate()
            }

            self.isUploading() ? completion(false) : completion(true)
        }
    }

    private func composeContent(audioVideoUrl: URL?, draw:  CALayer?, aditionalLayers: [CALayer], backgroundImage: UIImage?, completion: @escaping (URL) -> Void) {
        if let _audioVideoUrl = audioVideoUrl {
            //case videofile with additional layers
            
            self.videoComposer.mergeVideo(url: _audioVideoUrl, image: backgroundImage, draw: draw,
                                          aditionalLayers: aditionalLayers, completion: completion)
            
        } else {
            if let img = backgroundImage {
                initiate(image: img)
            }
        }
    }

    private func isUploading() -> Bool {
        let isUploading = !progressCellsIsUploadingStatus.filter { _, value in
            value == true }.isEmpty
        return isUploading
    }
    
    ///ImageWriteToSavedPhotosAlbum
    func initiate(image: UIImage) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.labeledSuccess(title: nil, subtitle: "Photo saved successfully"), delay: 1.0)
        }
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    }

    
}
