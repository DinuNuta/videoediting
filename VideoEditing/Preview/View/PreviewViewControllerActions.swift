//
//  PreviewViewControllerActions.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 21.09.2021.
//

import Foundation
import UIKit

extension PreviewViewController {

    @IBAction func saveAction(_ sender: Any) {
//        guard let image = getCapturedImage() else { return }
//        presenter.initiate(action: image, draw: penOverlay, textOverlay: nil)
        presenter.initiate(action: nil, draw: penOverlay, textOverlay: getTextOverlay())
//        presenter.initiate(action: nil, draw: getTextOverlay(), textOverlay: nil)
    }

    private func getCapturedImage() -> UIImage? {
        if didAddTextOverlay {
            let textLayersView = UIView.init(frame: canvasView.frame)
            textLayersView.backgroundColor = .clear
            view.subviews.filter { $0 is UITextView }.forEach {textLayersView.addSubview($0)}
            canvasView.addSubview(textLayersView)
            let textimg = textLayersView.snapshot()
            print(textimg)
        }
        return canvasView.snapshot()
    }
    
    private func getTextOverlay()-> CALayer?{
        
        if didAddTextOverlay {
            let textLayersView = UIView.init(frame: canvasView.frame)
            textLayersView.backgroundColor = .clear
            var textlayers = CALayer()
            textlayers.frame = canvasView.frame
            
            view.subviews.filter { $0 is UITextView }.forEach {
                textLayersView.addSubview($0)
//                if let textview = $0 as? UITextView{
//                    let textlayer = CATextLayer()
//                    textlayer.frame = textview.frame
//                    textlayer.string = textview.text
//                    textlayer.font = textview.font
//                    textlayer.alignmentMode = layerTextAlignment(textview.textAlignment)
//                    textlayer.foregroundColor = textview.textColor?.cgColor ?? UIColor.white.cgColor
//                    textlayer.backgroundColor = textview.backgroundColor?.cgColor
//                    textlayer.transform = textview.transform3D
////                    textlayer.shouldRasterize = true
//                    textlayer.rasterizationScale = UIScreen.main.scale
//
//                    textlayers.addSublayer(textlayer)
//                }
            }
            
            canvasView.addSubview(textLayersView)
            
            return textLayersView.layer
//            return textlayers
        }
        
        return nil
    }

    private func layerTextAlignment(_ alignemnt: NSTextAlignment) -> CATextLayerAlignmentMode{
        switch alignemnt {
        case .center:
            return .center
        case .left:
            return .left
        case .right:
            return .right
        case .justified:
            return .justified
        case .natural:
            return .natural
        @unknown default:
               return .center
        }
    }
    
    @IBAction func playPauseAction(_ sender: Any) {
        presenter.changePlayerState()
    }

    @IBAction func backAction(_ sender: Any) {
        textOverlay = nil
        dismiss(animated: true, completion: nil)
    }

    // TOOLS
    @IBAction func undoAction(_ sender: Any) {
        undoManipulationManager.removeLast()
    }

    @IBAction func layersAction(_ sender: Any) {
//        layersView?.setup()
    }

    @IBAction func textAction(_ sender: Any) {
        triggerToolIndicator(tool: .text, delegate: textOverlay)
    }

    @IBAction func cutAction(_ sender: Any) {
        triggerToolIndicator(tool: .scissors, delegate: draggableImageView)
    }

    @IBAction func drawAction(_ sender: Any) {
        // TODO
        if penOverlay == nil {
            let pen = DrawableCanvasViewImp.init(frame: canvasView.frame)
            self.canvasView.addSubview(pen)
            penOverlay = pen
            penOverlay?.isUserInteractionEnabled = false
            penOverlay?.setup(toolViewControllerDelegate: self, undoManipulationManager: undoManipulationManager)
        }
        textOverlay?.disable(true)
        triggerToolIndicator(tool: .draw, delegate: penOverlay!)
    }

    func triggerToolIndicator(tool: ToolSet, delegate: ToolTriggerDelegate?) {
        guard let isActive = delegate?.isActive() else { return }
        isActive ? deactivate(tool: tool, delegate: delegate) : activate(tool: tool, delegate: delegate)
    }

    private func activate(tool: ToolSet, delegate: ToolTriggerDelegate?) {
        guard let toolView = toolsContainerView.viewWithTag(tool.rawValue) else { return }

        delegate?.activate(true)
        deactivateUnusedTools(whileUsing: tool)
        toolView.transform = view.transform.scaledBy(x: 1.5, y: 1.5)
        shouldHideBackButton(true)
        activateUndoButton()
        if tool == .draw {textOverlay?.disable(true)}
    }

    private func deactivate(tool: ToolSet, delegate: ToolTriggerDelegate?) {
        guard let toolView = toolsContainerView.viewWithTag(tool.rawValue) else { return }

        delegate?.activate(false)
        deactivateUnusedTools()
        toolView.transform = CGAffineTransform.identity
        shouldHideBackButton(false)
        activateUndoButton(false)
        if tool == .draw {textOverlay?.disable(false)}
    }

    func deactivateTextOverlay(delegate: ToolTriggerDelegate?) {
        deactivate(tool: .text, delegate: delegate)
    }

    func deactivateUnusedToolsWhileUsingTextOverlay() {
        deactivateUnusedTools(whileUsing: .text)
    }

    private func shouldHideBackButton(_ shouldHide: Bool) {
        guard let backButton = toolButtons.filter({ $0.tag == constants.preview_backTag }).first else { return }
        backButton.isHidden = shouldHide
    }

    private func deactivateUnusedTools(whileUsing tool: ToolSet = .none) {
        switch tool {
        case .layers:
            filters?.activate(false)
            draggableImageView?.activate(false)
            textOverlay?.activate(false)
        case .scissors:
            filters?.activate(false)
            penOverlay?.activate(false)
        case .text:
            filters?.activate(false)
            draggableImageView?.activate(false)
        //            penOverlay?.activate(false)
        case .draw:
            filters?.activate(false)
            draggableImageView?.activate(false)
        case .none:
            filters?.activate(true)
            draggableImageView?.activate(true)
        default: break
        }
    }

    private func activateUndoButton(_ shouldActivate: Bool = true) {
        guard let undoButton = toolButtons.filter({ $0.tag == ToolSet.undo.rawValue }).first else { return }
        undoButton.isEnabled = !shouldActivate
    }
}
