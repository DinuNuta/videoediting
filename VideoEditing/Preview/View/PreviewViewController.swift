//
//  PreviewViewController.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 21.09.2021.
//

import Foundation
import UIKit
import AVFoundation

protocol PreviewView: AnyObject {
    func displayAlert(_ message: String)
}

enum ToolSet: Int {
    case undo = 101
    case layers = 102
    case text = 103
    case scissors = 104
    case draw = 105
    case none = 199
}

class PreviewViewController: UIViewController, PreviewView, UIGestureRecognizerDelegate, ToolViewDelegate, AlertErrorPresenting {

    @IBOutlet weak var scissorsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButtonXConstraint: NSLayoutConstraint!
    @IBOutlet weak var toolButtonsTrailingConstraint: NSLayoutConstraint!

    @IBOutlet var toolButtons: [UIButton]!
    @IBOutlet var actionButtons: [UIButton]!
    @IBOutlet var actionButtonsLabels: [UILabel]!

    @IBOutlet weak var toolsContainerView: UIView!
    @IBOutlet weak var capturedImageView: UIImageView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var filterTitle: UILabel!

    @IBOutlet weak var canvasView: UIView!

    var presenter: PreviewPresenter!
    var undoManipulationManager: UndoManipulationManager!

    var playerLayer: AVPlayerLayer?

    var filters: Filters?
    var textOverlay: TextOverlay?
    weak var penOverlay: DrawableCanvasViewImp?
    var draggableImageView: DraggableImageView?
    
    weak var colorPalette: ColorPaletteImp?
    var didAddTextOverlay = false
    var didSelectAnimatedImage = false
    var shouldShowPlayPauseButton = true
    var isFirstPlayButtonAppearance = true

    var imageResult: UIImage?

    var viewDidSetup = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        if !viewDidSetup {
            viewDidSetup = true
            addTapGesture()
            setupUndoManipulationManager()
            setupOutlets()
            setupImageView()
            setupPreviewToolset()
            setupComponents()

            disableShots()
            renamePost()
        }
    }

    private func disableShots() {
        guard let shotButton = actionButtons.filter({ $0.tag == 2 }).first else { return }
        shotButton.isHidden = true
        shotButton.isEnabled = false

        guard let shotButtonLabel = actionButtonsLabels.filter({ $0.tag == 2 }).first else { return }
        shotButtonLabel.isHidden = true
    }

    private func renamePost() {
        guard let postButton = actionButtons.filter({ $0.tag == 1 }).first else { return }
        postButton.setImage(UIImage(named: "camera_upload"), for: .normal)

        guard let postButtonLabel = actionButtonsLabels.filter({ $0.tag == 1 }).first else { return }
        postButtonLabel.text = "upload".uppercased()
    }

    private func disableTools() {
        toolButtons.filter { $0.tag != ToolSet.undo.rawValue && $0.tag != constants.preview_backTag }.forEach {
            $0.isHidden = true
            $0.isEnabled = false
        }
    }

    deinit {

    }
    
    private func setupPreviewToolset() {
        scissorsHeightConstraint.constant = presenter.isAudioVideoAvailable() ? 0 : 64
        playPauseButton.isHidden = !presenter.isAudioVideoAvailable()

        if !playPauseButton.isHidden { playPauseButtonAppearance() }

        if presenter.isAudioVideoAvailable() {
            setupAudioVideoPlayer()
        }

        toolButtons.filter { $0.tag == ToolSet.undo.rawValue }.first?.isHidden = !undoManipulationManager.didManipulate
    }

    private func setupAudioVideoPlayer() {
        playerLayer = presenter.getPlayerLayer(for: capturedImageView.frame)
        if let layer = playerLayer {
            self.capturedImageView.layer.addSublayer(layer)
        }
    }

    private func setupOutlets() {
        actionButtonsLabels.forEach {
            switch $0.tag {
            case 0: $0.text = "save".uppercased()
            case 1: $0.text = "post".uppercased()
            case 2: $0.text = "shot".uppercased()
            default: break
            }
        }
        capturedImageView.image = presenter.backgroundImage
    }

    private func setupImageView() {
        guard let backgroundImage = presenter.backgroundImage else { return }
        draggableImageView = DraggableImageView(baseView: view, image: backgroundImage, undoManipulationManager: undoManipulationManager)
        draggableImageView?.setupConstraints(backgroundView: capturedImageView)
    }

    private func setupComponents() {
        filters = Filters(delegateView: view, primeImageView: draggableImageView, filterTitleLabel: filterTitle)
        textOverlay = TextOverlay(toolViewControllerDelegate: self, undoManipulationManager: undoManipulationManager)
    }

    private func setupUndoManipulationManager() {
        undoManipulationManager = UndoManipulationManagerImp(delegate: self)
    }

    func shouldHidePreviewTools(_ shouldHide: Bool) {

        let adjustment: CGFloat = shouldHide ? -constants.textOverlay_adjustment : 0

        backButtonXConstraint.constant = adjustment
        toolButtonsTrailingConstraint.constant = adjustment

        UIView.animate(withDuration: constants.textOverlay_basicAnimationDuration,
                       delay: constants.textOverlay_basicAnimationDelay,
                       options: .curveEaseIn,
                       animations: {

                        self.view.layoutIfNeeded()

                        self.actionButtons.forEach { $0.alpha = shouldHide ? 0 : 1 }
                        self.actionButtonsLabels.forEach { $0.alpha = shouldHide ? 0 : 1 }
        },
                       completion: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        if playerLayer != nil {
            playerLayer?.removeObserver(self, forKeyPath: "videoRect")
        }
    }

    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey: Any]?,
                               context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            if playerLayer?.player?.rate == 0.0 {
                playPauseButton.setImage(UIImage(named: "play"), for: .normal)
            } else {
                playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            }
        }
        if keyPath == "videoRect" {
            guard let layer = playerLayer else {
                return
            }
            let y = (capturedImageView.frame.height - layer.videoRect.height) / 2
            playerLayer?.frame = CGRect(x: layer.videoRect.origin.x,
                                        y: y,
                                        width: layer.videoRect.width,
                                        height: layer.videoRect.height)
        }
    }

    private func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
    }

    @objc
    private func handleTapGesture(_ sender: UITapGestureRecognizer) {
        if !playPauseButton.isHidden {
            playPauseButtonAppearance()
        }
    }

    private func playPauseButtonAppearance() {
        if shouldShowPlayPauseButton {
            shouldShowPlayPauseButton(true)
            shouldShowPlayPauseButton = false

            if isFirstPlayButtonAppearance {
                isFirstPlayButtonAppearance = false

                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                    self.shouldShowPlayPauseButton(false)
                    self.shouldShowPlayPauseButton = true
                }
            }

        } else {
            shouldShowPlayPauseButton(false)
            shouldShowPlayPauseButton = true
        }
    }

    private func shouldShowPlayPauseButton(_ shouldShow: Bool) {

        UIView.animate(withDuration: constants.preview_playPauseButtonAnimationDuration,
                       delay: constants.preview_playPauseButtonAnimationDelay,
                       options: .curveEaseIn,
                       animations: {
                        self.playPauseButton.alpha = shouldShow ? 1 : 0
        }, completion: nil)
    }


    func displayAlert(_ message: String) {
        showAlert(title: "Video Editing", message: message)
    }
}

protocol ToolViewDelegate: AnyObject, ColorPalatteContaining {
    var toolButtons: [UIButton]! { get }
    var didAddTextOverlay: Bool { get set }
    var textOverlay: TextOverlay? { get }
    var didSelectAnimatedImage: Bool { get set }
    var presenter: PreviewPresenter! { get }
    var undoManipulationManager: UndoManipulationManager! { get }

    func triggerToolIndicator(tool: ToolSet, delegate: ToolTriggerDelegate?)
    func deactivateTextOverlay(delegate: ToolTriggerDelegate?)
    func deactivateUnusedToolsWhileUsingTextOverlay()
    func shouldHidePreviewTools(_ shouldHide: Bool)
}

protocol ToolTriggerDelegate {
    func isActive() -> Bool
    func activate(_ shouldActivate: Bool)
}
