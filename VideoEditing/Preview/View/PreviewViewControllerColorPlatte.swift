//
//  PreviewViewControllerColorPlatte.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 21.09.2021.
//

import Foundation
import UIKit

protocol ColorPalatteContaining {
    var colorPalette: ColorPaletteImp? {get set}

    func subscribeToColorPalete(item: ColorPaletteDelegate?)
    func colorPaletteShould(Show show: Bool)
}

extension PreviewViewController: ColorPalatteContaining {
    func subscribeToColorPalete(item: ColorPaletteDelegate?) {

        if item == nil {
            view.subviews.filter { $0.tag == constants.textOverlay_colorsTag }.first?.removeFromSuperview()
            return
        }
        guard let palette = self.colorPalette else {
            self.colorPalette = ColorPaletteImp(toolViewControllerDelegate: self, delegate: item)
            self.colorPalette?.shouldShow(show: true)
            return
        }

        if item != nil,
            !view.subviews.contains(where: {$0 is ColorPalatteContaining}) {
            view.addSubview(palette)
        }

        palette.colorDelegate = item
        palette.resetPalette()
        palette.shouldShow(show: item != nil)
    }

    func colorPaletteShould(Show show: Bool) {
        self.colorPalette?.shouldShow(show: show)
    }
}
