//
//  PreviewViewControllerUndoExtension.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 21.09.2021.
//

import Foundation
import UIKit

extension PreviewViewController: ManipulationComponent {

    func shouldShowUndoButton(_ shouldShow: Bool) {
        toolButtons.filter { $0.tag == ToolSet.undo.rawValue }.first?.isHidden = !shouldShow
    }

    func remove(manipulation: UndoManipulationManagerImp.Manipulation) {
        switch manipulation {
        case .added(component: let component): undoAdd(component)
        case .changedAlignment(component: let component, alignment: let alignment): undoAlignment(component, alignment: alignment)
        case .changedFontSize(component: let component, fontSize: let fontSize): undoFontSize(component, fontSize: fontSize)
        case .changedPosition(component: let component, center: let center): undoChangePosition(component, center: center)
        case .changedTextOverlayBackgroundColor(component: let component, color: let color): undoBackgroundColor(component, color: color)
        case .changedTransform(component: let component, transform: let transform): undoChangeRotation(component, transform: transform)
        case .changedValue(component: let component, value: let value): undoChangeValue(component, value: value)
        case .removed(component: let component): undoRemove(component)
        }
    }

    private func undoAdd(_ component: UndoComponent) {
        switch component {
        case .textOverlay: textOverlay?.undoAdd()
        case .draw: penOverlay?.undo()
        default: break
        }
    }

    private func undoAlignment(_ component: UndoComponent, alignment: NSTextAlignment) {
        switch component {
        case .textOverlay(tag: let tag): textOverlay?.undo(tag: tag, alignment: alignment)
        default: break
        }
    }

    private func undoFontSize(_ component: UndoComponent, fontSize: CGFloat) {
        switch component {
        case .textOverlay(tag: let tag): textOverlay?.undo(tag: tag, fontSize: fontSize)
        default: break
        }
    }

    private func undoBackgroundColor(_ component: UndoComponent, color: UIColor) {
        switch component {
        case .textOverlay(tag: let tag): textOverlay?.undo(tag: tag, backgroundColor: color)
        default: break
        }
    }

    private func undoChangePosition(_ component: UndoComponent, center: CGPoint) {
        switch component {
        case .image: draggableImageView?.undoChangePosition(center: center)
        case .textOverlay(tag: let tag): textOverlay?.undoChangePosition(tag: tag, center: center)
        default: break
        }
    }

    private func undoChangeRotation(_ component: UndoComponent, transform: CGAffineTransform) {
        switch component {
        case .image: draggableImageView?.undoChangeTransform(transform: transform)
        default: break
        }
    }

    private func undoChangeScale(_ component: UndoComponent, transform: CGAffineTransform) {
        switch component {
        case .image: draggableImageView?.undoChangeTransform(transform: transform)
        default: break
        }
    }

    private func undoChangeValue(_ component: UndoComponent, value: String) {
        switch component {
        case .textOverlay(tag: let tag): textOverlay?.undoChange(tag: tag, value: value)
        default: break
        }
    }

    private func undoRemove(_ component: UndoComponent) {
        switch component {
        case .textOverlay: textOverlay?.undoRemove()
        default: break
        }
    }

    func restore(manipulation: UndoManipulationManagerImp.Manipulation) {
        switch manipulation {
        case .added(component: let component): restoreAdd(component)
        default: break
        }
    }

    private func restoreAdd(_ component: UndoComponent) {
        switch component {
        default: break
        }
    }
}
