//
//  PreviewBuilder.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit
import Swinject


protocol PreviewBuilder {
    func buildPreviewViewController(image: UIImage,
                                    delegate: PhotoVideoCaptureDelegate?,
                                    sender: Any?) -> PreviewViewController!
    func buildPreviewViewController(audioVideoUrl: URL?,
                                    image: UIImage?,
                                    delegate: PhotoVideoCaptureDelegate?,
                                    sender: Any?) -> PreviewViewController!
}

class PreviewDefaultBuilder: PreviewBuilder {

    let container = Container()

    func buildPreviewViewController(image: UIImage,
                                    delegate: PhotoVideoCaptureDelegate? = nil,
                                    sender: Any? = nil) -> PreviewViewController! {

        return buildPreviewViewController(audioVideoUrl: nil,
                                          image: image,
                                          delegate: delegate,
                                          sender: sender)
    }

    func buildPreviewViewController(audioVideoUrl: URL?,
                                    image: UIImage?,
                                    delegate: PhotoVideoCaptureDelegate? = nil,
                                    sender: Any? = nil) -> PreviewViewController! {


        container.register(PreviewViewController.self) { _ in

            PreviewViewController(nibName: String(describing: PreviewViewController.self), bundle: .main)

        }.initCompleted { r, h in

            h.presenter = r.resolve(PreviewPresenter.self)
        }

        container.register(PreviewRouter.self) { c in
            let router = PreviewRouterImp()
            router.viewController = c.resolve(PreviewViewController.self)!
            return router
        }

        container.register(PreviewPresenter.self) { c in
            PreviewPresenterImp(view: c.resolve(PreviewViewController.self)!,
                                router: c.resolve(PreviewRouter.self)!,
                                audioVideoUrl: audioVideoUrl,
                                image: image,
                                videoComposer: VideoComposerImp())
        }
        return container.resolve(PreviewViewController.self)!
    }

    deinit {
        container.removeAll()
    }
}
