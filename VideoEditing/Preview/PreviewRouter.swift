//
//  PreviewRouter.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit

protocol PreviewRouter {
    func close()
}

class PreviewRouterImp: PreviewRouter {

    weak var viewController: PreviewViewController!
    
    func close(){
        viewController.textOverlay = nil
        if let vc = viewController.presentingViewController?.presentingViewController {
            vc.dismiss(animated: true)
        } else {
            viewController.dismiss(animated: true)
        }
    }
}
