//
//  VideoWriter.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import AVFoundation
import VideoToolbox
import UIKit

extension VideoWriter: FileManagerContaining {}

class VideoWriter {
    var assetWritter: AVAssetWriter!
    var assetWritterInput: AVAssetWriterInput!
    var pixelBufferAdaptor: AVAssetWriterInputPixelBufferAdaptor!
    var outputUrl: URL?

    var frameNumber = 0
    var images: [UIImage]
    let fps: Int32

    init(images: [UIImage], fps: Int32) {
        self.images = images
        self.fps = fps
        self.outputUrl = setOutputUrl()
    }

    var isReadyForData: Bool {
        return assetWritterInput.isReadyForMoreMediaData
    }

    func configure(with renderSize: CGSize) {
        // Setup assetWriter & assetWritterInput
        self.outputUrl = setOutputUrl()
        guard let outputUrl = outputUrl else { return }
        assetWritter = try? AVAssetWriter(url: outputUrl, fileType: .mov)
        let outputSettings: [String: Any] = [AVVideoCodecKey: AVVideoCodecType.h264,
                                             AVVideoWidthKey: renderSize.width,
                                             AVVideoHeightKey: renderSize.height]

        assetWritterInput = AVAssetWriterInput(mediaType: .video, outputSettings: outputSettings)

        if assetWritter.canAdd(assetWritterInput) { assetWritter.add(assetWritterInput) }

        // Setup pixelBufferAdaptor
        let sourcePixelBufferAttributes = [kCVPixelBufferPixelFormatTypeKey as String: NSNumber(value: kCVPixelFormatType_32ARGB),
                                           kCVPixelBufferWidthKey as String: NSNumber(value: Float(renderSize.width)),
                                           kCVPixelBufferHeightKey as String: NSNumber(value: Float(renderSize.height))]
        pixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: assetWritterInput, sourcePixelBufferAttributes: sourcePixelBufferAttributes)

        // Start session
        if !assetWritter.startWriting() { return }

        assetWritter.startSession(atSourceTime: .zero)
    }

    func write(croppedBackgroundImage: UIImage, _ completion: ((URL) -> Void)?) {

        let queue = DispatchQueue(label: "mediaInputQueue")
        assetWritterInput.requestMediaDataWhenReady(on: queue) {
            if self.appendPixelBuffers(croppedBackgroundImage: croppedBackgroundImage) {
                self.assetWritterInput.markAsFinished()
                self.assetWritter.finishWriting {
                    self.outputUrl.flatMap {(completion?($0))}
                }
            }
        }
    }

    func addImage(image: UIImage,
                  withPresentationTime presentationTime: CMTime,
                  croppedBackgroundImage: UIImage) -> Bool {

        guard let imageWithBackgroundImage = drawBackgroundImageInImage(backgroundImage: croppedBackgroundImage,
                                                                        mainImage: image) else { return false }
        guard let pixelBuffer = buffer(from: imageWithBackgroundImage) else { return false }
        return pixelBufferAdaptor.append(pixelBuffer, withPresentationTime: presentationTime)
    }

    private func drawBackgroundImageInImage(backgroundImage: UIImage, mainImage: UIImage) -> UIImage? {

        let rect = CGRect(origin: .zero, size: backgroundImage.size)

        UIGraphicsBeginImageContextWithOptions(backgroundImage.size, false, 0.0)

        backgroundImage.draw(in: rect)
        mainImage.draw(in: rect)

        let imageWithBackgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return imageWithBackgroundImage
    }

    func buffer(from image: UIImage) -> CVPixelBuffer? {
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer: CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(image.size.width), Int(image.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        guard status == kCVReturnSuccess else {
            return nil
        }
        //kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange
        CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()

        let context = CGContext(data: pixelData,
                                width: Int(image.size.width),
                                height: Int(image.size.height),
                                bitsPerComponent: 8,
                                bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!),
                                space: rgbColorSpace,
                                bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)

        context?.setAlpha(0.01)
        context?.setFillColor(UIColor.clear.cgColor)
        context?.translateBy(x: 0, y: image.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)

        UIGraphicsPushContext(context!)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        return pixelBuffer
    }

}

extension VideoWriter {
    func appendPixelBuffers(croppedBackgroundImage: UIImage) -> Bool {
        let frameDuration = CMTimeMake(value: Int64(constants.gif_kTimescale / fps), timescale: constants.gif_kTimescale)

        while !images.isEmpty {
            if !isReadyForData {
                return false
            }

            let image = images.removeFirst()
            let presentationTime = CMTimeMultiply(frameDuration, multiplier: Int32(frameNumber))
            let success = addImage(image: image, withPresentationTime: presentationTime, croppedBackgroundImage: croppedBackgroundImage)

            frameNumber += 1
        }
        return true
    }
}

//fileprivate extension UIImage {
//    func toCVPixelBuffer() -> CVPixelBuffer? {
//        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
//        var pixelBuffer : CVPixelBuffer?
//        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(self.size.width), Int(self.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
//        guard status == kCVReturnSuccess else {
//            return nil
//        }
//
//        if let pixelBuffer = pixelBuffer {
//            CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
//            let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer)
//
//            let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
//            let context = CGContext(data: pixelData, width: Int(self.size.width), height: Int(self.size.height), bitsPerComponent: 8,
//            bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
//
//            context?.translateBy(x: 0, y: self.size.height)
//            context?.scaleBy(x: 1.0, y: -1.0)
//
//            UIGraphicsPushContext(context!)
//            self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
//            UIGraphicsPopContext()
//            CVPixelBufferUnlockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
//
//            return pixelBuffer
//        }
//
//        return nil
//    }
//}
