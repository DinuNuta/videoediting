//
//  VideoComposer.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation
import UIKit
import AVFoundation
import AssetsLibrary

protocol VideoComposer {
//    func mergeVideo(url: URL?, aditionalLayer: CALayer?, completion: @escaping (URL) -> Void)
    func mergeVideo(url: URL?, image: UIImage?, draw: CALayer?, aditionalLayers: [CALayer], completion: @escaping (URL) -> Void)
    func merge(image: UIImage?, completion: @escaping (URL) -> Void)
}

class VideoComposerImp: VideoComposer {

    var renderSize: CGSize = UIScreen.main.nativeBounds.size

    func merge(image: UIImage?=nil, completion: @escaping (URL) -> Void) {

        let mixComposition = AVMutableComposition()
        
        let layerInstructions: [AVMutableVideoCompositionLayerInstruction] = []
        //TODO: ADD TIME composition
        //1.case video base time
        //2.case longer gif
        guard let compositionVideoTrack: AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: CMPersistentTrackID(1)) else {return}
        // get video track
        //add base video track
        let videoLayer = CALayer()
        videoLayer.backgroundColor = UIColor.clear.cgColor
        //        videoLayer.opacity = 0.8

        //compose new video
        let mainComposition = AVMutableVideoComposition()
        mainComposition.renderSize = self.renderSize
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)

        //add layer for background image
        if let img = image {
            let backgroundImageLayer = CALayer()
            let imgLocal = fixedOrientation(image: img)?.cgImage
            backgroundImageLayer.contents = imgLocal
            backgroundImageLayer.frame = CGRect(origin: .zero, size: renderSize)
            backgroundImageLayer.isGeometryFlipped = true
            backgroundImageLayer.backgroundColor = UIColor.clear.cgColor
            backgroundImageLayer.masksToBounds = true
            backgroundImageLayer.addSublayer(videoLayer)

            let parentLayer = CALayer()
            parentLayer.frame = CGRect(origin: .zero, size: renderSize)
            parentLayer.backgroundColor = UIColor.clear.cgColor
            parentLayer.addSublayer(backgroundImageLayer)
            mainComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
            //            mainComposition.animationTool = AVVideoCompositionCoreAnimationTool(additionalLayer: parentLayer, asTrackID: CMPersistentTrackID(layersCount+1))

        }

        //TODO: add animaitonTool to insert textlayer on video
        //            mainComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: animatedImagesLayer, in: parentLayer)

        //set layer instructions
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: mixComposition.duration)
        mainInstruction.backgroundColor = UIColor.clear.cgColor
        mainInstruction.layerInstructions = layerInstructions
        mainComposition.instructions = [mainInstruction]

        //export video
        exportAsset(mixComposition: mixComposition, videoComposition: mainComposition) {(url) in
            completion(url)
        }

    }

    func mergeVideo(url: URL?=nil, image: UIImage?, draw: CALayer?, aditionalLayers: [CALayer]=[], completion: @escaping (URL) -> Void) {
        // get video track
        var mixComposition = AVMutableComposition()
        var layerInstructions: [AVMutableVideoCompositionLayerInstruction] = []

        guard let compositionVideoTrack: AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video,
                                                                                                    preferredTrackID: CMPersistentTrackID(2))
            else {return}

        //add base video track
        if let url = url {
            let vidAsset = AVURLAsset(url: url, options: nil)
            let vtrack =  vidAsset.tracks(withMediaType: .video)
            let videoTrack: AVAssetTrack = vtrack[0]
            let audioTrack: AVAssetTrack? = vidAsset.tracks(withMediaType: .audio).first
            let vid_timerange = CMTimeRangeMake(start: CMTime.zero, duration: vidAsset.duration)

            do {
                try compositionVideoTrack.insertTimeRange(vid_timerange, of: videoTrack, at: .zero)
                if let audioTrack = audioTrack {
                    let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
                    try audioCompositionTrack?.insertTimeRange(vid_timerange, of: audioTrack, at: .zero)
                }
                compositionVideoTrack.preferredTransform = videoTrack.preferredTransform
            } catch {

            }

            let layerInstruction = videoCompositionInstructionForTrack(track: compositionVideoTrack, asset: vidAsset)
            layerInstruction.trackID = CMPersistentTrackID(2)
            layerInstructions.append(layerInstruction)
        }

        //compose new video
        let mainComposition = AVMutableVideoComposition()
        mainComposition.renderSize = self.renderSize
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)

        //TODO: add animaitonTool to insert textlayer on video
        //            mainComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: animatedImagesLayer, in: parentLayer)
        //add base video track

        //add layer on video
        if draw != nil ||
            !aditionalLayers.isEmpty {
            
            let parentLayer = CALayer()
            parentLayer.frame = CGRect(origin: .zero, size: renderSize)
            parentLayer.backgroundColor = UIColor.clear.cgColor
            
            
            if let _draw = draw{
                let drawLayer = CALayer()
                drawLayer.frame = CGRect(origin: .zero, size: renderSize)
                drawLayer.contents = _draw.contents
                parentLayer.addSublayer(drawLayer)
            }
            
//            aditionalLayers.forEach { layer in
//                parentLayer.addSublayer(layer)
//            }
            //---
            
            if let layer = aditionalLayers.first{
//                parentLayer.addSublayer(layer)
                UIGraphicsBeginImageContext(renderSize)
                guard  let ctx = UIGraphicsGetCurrentContext() else {return}
                layer.render(in: ctx)
//                layer.sublayers?.dropFirst().forEach({$0.render(in: ctx)})
                let renderedImg = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                let aditionalLayer = CALayer()
                aditionalLayer.frame = CGRect(origin: .zero, size: renderSize)

                aditionalLayer.contents = renderedImg?.cgImage
                aditionalLayer.backgroundColor = UIColor.clear.cgColor
                aditionalLayer.masksToBounds = true
                parentLayer.addSublayer(aditionalLayer)
                
            }
            //---
            
            mainComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: parentLayer, in: parentLayer)
//            mainComposition.animationTool = AVVideoCompositionCoreAnimationTool(additionalLayer: parentLayer, asTrackID: CMPersistentTrackID(3))
        }
      
        //set layer instructions
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: mixComposition.duration)
        mainInstruction.backgroundColor = UIColor.clear.cgColor
        mainInstruction.layerInstructions = layerInstructions
        mainComposition.instructions = [mainInstruction]

        //export video
        exportAsset(mixComposition: mixComposition, videoComposition: mainComposition, completion)
    }
}

extension VideoComposerImp {
    fileprivate func getLayerInstruction(videoURL: URL, origin: CGPoint, videoSize: CGSize,
                                         transform: CGAffineTransform?,
                                         trackID: Int=0, mixComposition: inout AVMutableComposition) -> AVMutableVideoCompositionLayerInstruction? {

        let gifAsset = AVURLAsset(url: videoURL)
        var gifLayerInstruction = AVMutableVideoCompositionLayerInstruction()

        if let gifVideoTrack = gifAsset.tracks(withMediaType: .video).first,
            let gifCompositionTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: CMPersistentTrackID(trackID)) {
            do {
                var gifDuration = gifAsset.duration
                let videoDuration = mixComposition.duration
                try gifCompositionTrack.insertTimeRange(CMTimeRangeMake(start: .zero, duration: gifAsset.duration), of: gifVideoTrack, at: .zero)
                while CMTimeCompare(gifDuration, videoDuration) == -1 {
                    let total = CMTimeAdd(gifDuration, gifAsset.duration)
                    var duration = gifAsset.duration
                    if CMTimeCompare(total, mixComposition.duration)==1 {
                        duration = CMTimeSubtract(videoDuration, gifDuration)
                    }
                    try gifCompositionTrack.insertTimeRange(CMTimeRangeMake(start: .zero, duration: duration), of: gifVideoTrack, at: gifDuration)
                    gifDuration = CMTimeAdd(gifDuration, gifAsset.duration)
                }
                gifCompositionTrack.preferredTransform = gifVideoTrack.preferredTransform
                gifLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: gifCompositionTrack)
                gifLayerInstruction.trackID = CMPersistentTrackID(trackID)

                let moveFactor = CGAffineTransform(translationX: origin.x, y: origin.y)
                let trScale = CGAffineTransform.from(self.renderSize, to: videoSize)
                if let transform = transform {
                    let concat = trScale.concatenating(transform).concatenating(moveFactor)
                    gifLayerInstruction.setTransform(concat, at: .zero)
                } else {
                    gifLayerInstruction.setTransform(trScale.concatenating(moveFactor), at: .zero)
                }
                gifLayerInstruction.setCropRectangle(CGRect(x: 0, y: 0, width: gifVideoTrack.naturalSize.width, height: gifVideoTrack.naturalSize.height), at: .zero)
            } catch {

            }
        }
        return gifLayerInstruction
    }

    fileprivate func exportAsset(mixComposition: AVMutableComposition, videoComposition: AVMutableVideoComposition, _ completion: @escaping (URL) -> Void) {

        let assetExport = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)

        guard let outputUrl = setOutputUrl() else { return }

        if fileManager.fileExists(atPath: outputUrl.path) {
            do { try fileManager.removeItem(atPath: outputUrl.path) } catch { }
        }

        assetExport?.outputFileType = .mov
        assetExport?.outputURL = outputUrl
        assetExport?.shouldOptimizeForNetworkUse = true
        assetExport?.videoComposition = videoComposition

        assetExport?.exportAsynchronously {
            completion(outputUrl)
        }
    }

    fileprivate func videoCompositionInstructionForTrack(track: AVMutableCompositionTrack, asset: AVAsset) -> AVMutableVideoCompositionLayerInstruction {
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: .video)[0]
        var transform = assetTrack.preferredTransform

        let naturalRect = CGRect.init(origin: .zero, size: assetTrack.naturalSize)
        let newVideoRect = naturalRect.applying(transform)

        let from =  newVideoRect
        let to = renderSize
        let sx  = to.width / from.size.width
        let sy  = to.height / from.size.height
        let scale = CGAffineTransform(scaleX: sx, y: sy)
        transform = transform.concatenating(scale)
        instruction.setTransform(transform, at: .zero)
        return instruction
    }

    fileprivate func fixedOrientation(image: UIImage) -> UIImage? {
        if image.imageOrientation == .up { return image }
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        
        let rect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        image.draw(in: rect)
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return normalizedImage
    }
}

extension VideoComposerImp: FileManagerContaining {}

extension CGAffineTransform {

    /// Returns transform for  scale difference from two given size.
    static func from(_ from: CGSize, to: CGSize) -> CGAffineTransform {
        let sx  = to.width / from.width
        let sy  = to.height / from.height

        return CGAffineTransform(scaleX: sx, y: sy)
    }
}

//extension VideoComposerImp {
//    fileprivate func orientationFromTransform(transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
//        var assetOrientation = UIImage.Orientation.up
//        var isPortrait = false
//        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
//            assetOrientation = .right
//            isPortrait = true
//        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
//            assetOrientation = .left
//            isPortrait = true
//        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
//            assetOrientation = .up
//        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
//            assetOrientation = .down
//        }
//        return (assetOrientation, isPortrait)
//    }
//
//    fileprivate func videoCompositionInstructionForTrack(track: AVCompositionTrack, asset: AVAsset, standardSize:CGSize, atTime: CMTime) -> AVMutableVideoCompositionLayerInstruction {
//        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
//        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
//
//        let transform = assetTrack.preferredTransform
//        let assetInfo = orientationFromTransform(transform: transform)
//
//        var aspectFillRatio:CGFloat = 1
//        if assetTrack.naturalSize.height < assetTrack.naturalSize.width {
//            aspectFillRatio = standardSize.height / assetTrack.naturalSize.height
//        }
//        else {
//            aspectFillRatio = standardSize.width / assetTrack.naturalSize.width
//        }
//
//        if assetInfo.isPortrait {
//            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
//
//            let posX = standardSize.width/2 - (assetTrack.naturalSize.height * aspectFillRatio)/2
//            let posY = standardSize.height/2 - (assetTrack.naturalSize.width * aspectFillRatio)/2
//            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
//
//            instruction.setTransform(assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor), at: atTime)
//
//        } else {
//            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
//
//            let posX = standardSize.width/2 - (assetTrack.naturalSize.width * aspectFillRatio)/2
//            let posY = standardSize.height/2 - (assetTrack.naturalSize.height * aspectFillRatio)/2
//            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
//
//            var concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
//
//            if assetInfo.orientation == .down {
//                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
//                concat = fixUpsideDown.concatenating(scaleFactor).concatenating(moveFactor)
//            }
//
//            instruction.setTransform(concat, at: atTime)
//        }
//        return instruction
//    }
//
//    fileprivate func setOrientation(image:UIImage?, onLayer:CALayer, outputSize:CGSize) -> Void {
//        guard let image = image else { return }
//
//        if image.imageOrientation == UIImage.Orientation.up {
//            // Do nothing
//        }
//        else if image.imageOrientation == UIImage.Orientation.left {
//            let rotate = CGAffineTransform(rotationAngle: .pi/2)
//            onLayer.setAffineTransform(rotate)
//        }
//        else if image.imageOrientation == UIImage.Orientation.down {
//            let rotate = CGAffineTransform(rotationAngle: .pi)
//            onLayer.setAffineTransform(rotate)
//        }
//        else if image.imageOrientation == UIImage.Orientation.right {
//            let rotate = CGAffineTransform(rotationAngle: -.pi/2)
//            onLayer.setAffineTransform(rotate)
//        }
//    }
//
//}
