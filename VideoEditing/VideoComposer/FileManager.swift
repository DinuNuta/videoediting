//
//  FileManager.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 22.09.2021.
//

import Foundation

protocol FileManagerContaining: AnyObject {
    var fileManager: FileManager {get}

    func setOutputUrl() -> URL?
    func getOutputUrl() -> URL?

    func removeTempFiles()
}

extension FileManagerContaining {
    var fileManager: FileManager {
        return FileManager.default
    }

    func setOutputUrl() -> URL? {
        if let exportUrl = try? fileManager.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true) {
            return exportUrl.appendingPathComponent(NSUUID().uuidString).appendingPathExtension(constants.gif_videoExtension)
        }
        return nil
    }

    func getOutputUrl() -> URL? {
        guard let documentsUrl = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first else { return nil }
        let documentsPath = documentsUrl.path

        var names: [String] = []

        do { names = try fileManager.contentsOfDirectory(atPath: "\(documentsPath)") } catch { }

        guard let video = names.filter({ $0.hasSuffix(".\(constants.gif_videoExtension)") }).first else { return nil }
        let filePath = "\(documentsPath)/\(video)"
        let url = URL(fileURLWithPath: filePath)
        return url
    }

    func removeTempFiles() {
        guard let documentsUrl = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first else { return }
        let documentsPath = documentsUrl.path

        do {
            let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentsPath)")

            fileNames.filter { $0.hasSuffix(".\(constants.gif_videoExtension)") }.forEach {
                let filePath = "\(documentsPath)/\($0)"
                do { try fileManager.removeItem(atPath: filePath) } catch { }
            }

            let files = try fileManager.contentsOfDirectory(atPath: "\(documentsPath)")
        } catch {

        }
    }
}
