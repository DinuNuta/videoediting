//
//  UIView+Extensions.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

extension UIView {

    @available(iOS 11.0, *)
    func roundCorners(_ condition: DCRoundCorners, radius: CGFloat = 10) {
        var corners = CACornerMask()
        if condition.contains(.topLeft) {
            corners.insert(.layerMinXMinYCorner)
        }
        if condition.contains(.topRight) {
            corners.insert(.layerMaxXMinYCorner)
        }
        if condition.contains(.bottomLeft) {
            corners.insert(.layerMinXMaxYCorner)
        }
        if condition.contains(.bottomRight) {
            corners.insert(.layerMaxXMaxYCorner)
        }
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = corners
    }

    class DCRoundCorners: OptionSet {

        public let rawValue: Int
        required public init(rawValue: Int) { self.rawValue = rawValue }
        static public let topLeft = DCRoundCorners(rawValue: 1 << 0)
        static public let topRight = DCRoundCorners(rawValue: 1 << 1)
        static public let bottomLeft = DCRoundCorners(rawValue: 1 << 2)
        static public let bottomRight = DCRoundCorners(rawValue: 1 << 3)
        static public let all: DCRoundCorners = [.topLeft, .topRight, .bottomLeft, .bottomRight]
    }
}

extension UIView {

    func snapshot() -> UIImage? {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}


extension UICollectionView {

    func registerNibForCell(_ cellClass: AnyClass!) {
        let nibName = NSStringFromClass(cellClass.self).components(separatedBy: ".").last
        let cellNib = UINib(nibName: nibName!, bundle: Bundle.main) as UINib

        register(cellNib, forCellWithReuseIdentifier: nibName!)
    }
}

extension UITableView {

    func registerNibForCell(_ cellClass: AnyClass!) {
        let nibName = NSStringFromClass(cellClass.self).components(separatedBy: ".").last
        let cellNib = UINib(nibName: nibName!, bundle: Bundle.main) as UINib

        register(cellNib, forCellReuseIdentifier: nibName!)
    }
}



extension UIViewController {
    func hideKeyboardWhenFinishEditing() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboardFromOutside))
        view.addGestureRecognizer(tapGesture)
    }

    @objc
    func hideKeyboardFromOutside() {
        view.endEditing(true)
    }
}


