//
//  UIImage+Assets.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

extension UIImage {

    enum Assets: String {
        case a1
        case a2
        case a3
        case centerAlignment
        case leftAlignment
        case rightAlignment
        case upload

        func name() -> String {
            switch self {
            case .a1: return "A1"
            case .a2: return "A2"
            case .a3: return "A3"
            case .centerAlignment: return "center"
            case .leftAlignment: return "left_a"
            case .rightAlignment: return "right_a"
            case .upload: return "camera_upload"
            }
        }
    }

    convenience init!(asset: Assets) {
        self.init(named: asset.name())
    }
}
