//
//  Created by Coscodan Dinu on 3/17/17.
//  Copyright © 2017 Coscodan Dinu. All rights reserved.
//
import UIKit
import Foundation

open class CDAppearance: NSObject {
    @IBInspectable open var tintColor: UIColor?   //UIview Animatable
    @IBInspectable open var backgroundColor: UIColor?   //UIview Animatable

    @IBInspectable open var cornerRadius: CGFloat = 0   // CA Animatable

    @IBInspectable open var borderWidth: CGFloat = 0   // CA Animatable
    @IBInspectable open var borderColor: UIColor?   // CA Animatable

    @IBInspectable open var shadowColor: UIColor?    // CA Animatable
    @IBInspectable open var shadowPath: CGPath?    // CA Animatable
    @IBInspectable open var shadowOffset: CGSize = CGSize.zero   // CA Animatable
    @IBInspectable open var shadowOpacity: Float = 0   // CA Animatable
    @IBInspectable open var shadowRadius: CGFloat = 0  // CA Animatable
    @IBInspectable open var clipToBounds: Bool = false

    @IBInspectable open var round: Bool = false //CA Animatable
    @IBInspectable open var rotateAngle: CGFloat = 0

    public func setup<T: UIView>(view: T) {
        tintColor = view.tintColor
        backgroundColor = view.backgroundColor
        cornerRadius = view.cornerRadius
        borderWidth = view.borderWidth
        borderColor = view.borderColor
        shadowColor = view.shadowColor
        shadowPath = view.shadowPath
        shadowOffset = view.shadowOffset
        shadowOpacity = view.shadowOpacity
        shadowRadius = view.shadowRadius
        round = view.roundCorner
        clipToBounds = view.clipsToBounds
    }

    convenience public init(view: UIView) {
        self.init()
        setup(view: view)
    }
    ///Empty hook for configuration in subclass
    open func configureAppearance() {}

    public override init() {
        super.init()
        configureAppearance()
    }

    open override func awakeFromNib() {
        super.awakeFromNib()
        configureAppearance()
    }
}

public extension UIView {
    @IBOutlet var defaultAppearance: CDAppearance? {
        get {return CDAppearance(view: self)}
        set {newValue.flatMap {setAppearance(appearance: $0)}}
    }
}

public extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }

    @IBInspectable var borderColor: UIColor? {
        get {return layer.borderColor.flatMap {UIColor(cgColor: $0)} }
        set {layer.borderColor = newValue.flatMap {$0.cgColor}}
    }

    @IBInspectable var borderWidth: CGFloat {
        get {return layer.borderWidth}
        set {layer.borderWidth = newValue}
    }

    @IBInspectable var shadowColor: UIColor? {
        get {return layer.shadowColor.flatMap {UIColor(cgColor: $0)} }
        set {layer.shadowColor = newValue.flatMap {$0.cgColor}}
    }

    @IBInspectable var shadowOpacity: Float {
        get {return layer.shadowOpacity}
        set {layer.shadowOpacity = newValue}
    }

    @IBInspectable var shadowOffset: CGSize {
        get {return layer.shadowOffset}
        set {layer.shadowOffset = newValue}
    }

    @IBInspectable var shadowRadius: CGFloat {
        get {return layer.shadowRadius}
        set {layer.shadowRadius = newValue}
    }

    @IBInspectable var shadowPath: CGPath? {
        get {return layer.shadowPath}
        set {layer.shadowPath = newValue}
    }

    @IBInspectable var roundCorner: Bool {
        get { return bounds.width == bounds.height && cornerRadius == bounds.height/2 }
        set { cornerRadius = newValue ? bounds.height/2 : layer.cornerRadius }
    }

    @IBInspectable var rotateAngle: CGFloat {
        get { return 0 }
        set {
            //            layer.transform = CATransform3DMakeRotation(newValue, 0, 1, 1)
            let animation = CABasicAnimation(keyPath: "transform.rotation")

            animation.toValue = newValue
            animation.isRemovedOnCompletion = false
            animation.fillMode = .forwards

            self.layer.add(animation, forKey: nil)

        }
    }
}
