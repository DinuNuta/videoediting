//
//  AlertErrorPresenting.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit


public protocol AlertErrorPresenting {
    func showAlert(title: String, message: String)
    func showAlert(message: String, title: String?, ok: String, cancel: String, completion: (() -> Void)?)
    func showAlertError(error: Error)
    func showAlertError(alert: UIAlertController, animated: Bool, completion: (() -> Void)?)
    func didShowAlertError()
    var mainViewController: UIViewController? {get}
}

extension AlertErrorPresenting {
    func showAlertError(error: Error) {
        var title = "Error"
        var message = error.localizedDescription

        // MARK: Catch errore case
        switch error {

        case is DecodingError:
            let decodeError = error as! DecodingError
            message = decodeError.debugDescription
        default:
            message = error.localizedDescription
        }

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {(_: UIAlertAction!) in}))

        DispatchQueue.main.async {
            self.showAlertError(alert: alert, animated: true, completion: nil)
        }
        didShowAlertError()
    }

    func showAlertLocalize(message: String, title: String?=nil, ok: String = "OK", cancel: String = "cancel", completion: (() -> Void)? ) {
        showAlert(message: message, title: title, ok: ok, cancel: cancel, completion: completion)
    }

    func showAlert(message: String, title: String?=nil, ok: String = "OK", cancel: String = "cancel", completion: (() -> Void)? ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancel, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: ok, style: .default, handler: {(_: UIAlertAction!) in
            completion?()
        }))
        
        DispatchQueue.main.async {
            self.showAlertError(alert: alert, animated: true, completion: nil)
        }
    }

    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {(_: UIAlertAction!) in}))
        DispatchQueue.main.async {
            self.showAlertError(alert: alert, animated: true, completion: nil)
        }
        didShowAlertError()
    }

    func didShowAlertError() {}
}

public extension AlertErrorPresenting where Self: UIViewController {

    func showAlertError(alert: UIAlertController, animated: Bool=true, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            self.present(alert, animated: animated, completion: completion)
        }
        didShowAlertError()
    }

    var mainViewController: UIViewController? {
        get {
            return self
        }
    }
}

public extension AlertErrorPresenting {
    func showAlertError(alert: UIAlertController, animated: Bool=true, completion: (() -> Void)? = nil) {
        
        if let mainVC = UIApplication.topViewController() {
            DispatchQueue.main.async {
                mainVC.present(alert, animated: true, completion: nil)
            }
            didShowAlertError()
        }
    }

    var mainViewController: UIViewController? {
        get {
            return UIApplication.topViewController()
        }
    }
}

extension DecodingError {

    public var debugDescription: String {
        switch  self {
        case .dataCorrupted(let context):
            return NSLocalizedString(context.debugDescription, comment: "")
        case .keyNotFound(let key, let context):
            return "\(key.stringValue) - Key not Found. \n \(context.debugDescription)"
        case .typeMismatch(let type, let context):
            return "\(type.self) - Type Mismatch. \n \(context.debugDescription)"
        case .valueNotFound(let type, let context):
            return "\(type.self) - Value Not Found. \n \(context.debugDescription)"
        @unknown default:
            return NSLocalizedString("\(self.errorDescription ?? "Unknown Error")", comment: "")
        }
    }
}

// MARK: - Get current Top ViewController

public extension UIApplication {
    static func topViewController(base: UIViewController? = (UIApplication.shared.delegate)?.window??.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }

        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }

        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
