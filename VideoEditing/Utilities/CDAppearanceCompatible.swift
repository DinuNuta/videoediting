//
//  Created by Coscodan Dinu on 3/3/17.
//  Copyright © 2017 Coscodan Dinu. All rights reserved.
//

import UIKit

@objc public protocol CDAppearanceCompatible: AnyObject {
    var layer: CALayer {get}

    var cornerRadius: CGFloat {get set}
    var borderWidth: CGFloat {get set}
    var borderColor: UIColor? {get set}
    var shadowColor: UIColor? {get set}
    var shadowOpacity: Float {get set}
    var shadowOffset: CGSize {get set}
    var shadowRadius: CGFloat {get set}
    var shadowPath: CGPath? {get set}
    var roundCorner: Bool {get set}
    var rotateAngle: CGFloat {get set}

    @objc func setAppearance(appearance: CDAppearance?)
}

public extension UIView {
    @objc func setAppearance(appearance: CDAppearance?) {
        let appearance = appearance ?? CDAppearance()
        appearance.tintColor.flatMap { tintColor = $0 }
        backgroundColor = appearance.backgroundColor
        cornerRadius = appearance.cornerRadius
        borderColor = appearance.borderColor
        borderWidth = appearance.borderWidth
        shadowColor = appearance.shadowColor
        shadowOffset = appearance.shadowOffset
        shadowRadius = appearance.shadowRadius
        shadowPath = appearance.shadowPath
        shadowOpacity = appearance.shadowOpacity
        roundCorner = appearance.round
        clipsToBounds = appearance.clipToBounds
        rotateAngle = appearance.rotateAngle
    }
}
