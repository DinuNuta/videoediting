//
//  Constants.swift
//  VideoEditing
//
//  Created by Coscodan Dinu on 23.09.2021.
//

import Foundation
import UIKit

public let constants = Constants.shared

public struct Constants {

    static let shared = Constants()
    let gif_videoExtension = "mov"
    let gif_kTimescale: Int32 = 600
    
//    // TextOverlay
    let textOverlay_initialWidth: CGFloat = 20
    let textOverlay_sideInsets: CGFloat = 10
    let textOverlay_minFontSize: Float = 6
    let textOverlay_maxFontSize: Float = 50
    let textOverlay_standardFontSize: Float = 24
    let textOverlay_sliderHeightRotated: CGFloat = 30
    let textOverlay_sliderWidthRotated: CGFloat = 200
    let textOverlay_alignmentStates = 2
    let textOverlay_topViewMeasurement: CGFloat = 30
    let textOverlay_topViewTopArchon: CGFloat = -95
    let textOverlay_topViewLeftArchon: CGFloat = 10
    let textOverlay_manipulationViewWidth: CGFloat = 40
    let textOverlay_backgroundStates = 13
    let textOverlay_colorsHeight: CGFloat = 40
    let textOverlay_colorsItemMeasurement: CGFloat = 30
    let textOverlay_colorsItemInset: CGFloat = 5
    let textOverlay_colorsStates = 12
    let textOverlay_centerYLineWidth: CGFloat = 2
    let textOverlay_centerYLineTag = 99999
    let textOverlay_adjustment: CGFloat = 100
    let textOverlay_basicAnimationDuration: Double = 0.3
    let textOverlay_basicAnimationDelay: Double = 0.2
    let textOverlay_centerXAdjustment: CGFloat = 15
    let textOverlay_darkBackgroundTag = 88888
    let textOverlay_darkBackgroungAlpha: CGFloat = 0.5
    let textOverlay_cornerRadius: CGFloat = 5
    let textOverlay_symbolsTargetCount = 5
    let textOverlay_colorsTag = 1111
//
//    // PreviewView
    let preview_backTag = 184
    let preview_playPauseButtonAnimationDuration: Double = 0.5
    let preview_playPauseButtonAnimationDelay: Double = 0
//    //draw overlay
    let toolbarHeight_adjuststment: CGFloat = 100

    let drawLine_strokeWidth: Float = 6
    let drawLine_minStrokeWidth: Float = 10
    let drawLine_maxStrokeWidth: Float = 220
    let drawLine_maxEraserWidth: Float = 300

    private init() { }
}
